TableEntry = class TableEntry {

	static load() {

		this.fileMap = new Map(nn.map(e => e.crossFile).filter(Boolean).map(e => [e.__path, e]))
		this.notaMap = new Map(nn.map(e => [e.uniq, e]))
		const fnames = ['LANG', 'database', 'item-database', 'other', 'yatl']
		let entries = []
		for (let fname of fnames) {
			let data = fs.readFileSync(`./assets/editor/${fname}.tsv`, 'utf-8')
			data = data.split('\n').filter(Boolean).map(e => e.replace(/↵/g, '\n')).map(e => e.split('\t'))
			let header = data.shift()

			entries.push(data.map(e => new TableEntry(e, fname)))
		}
		log(entries)
		entries = entries.flat().filter(e => e.c)
		this.te = {}
		this.te.all = entries
		this.te.simple = entries.filter(e => e.simple)
		this.te.entries = entries.filter(e => !e.simple)
		this.te.missing = this.te.entries.filter(e => !e.c.etc.rus.length)
		this.te.okmissing = this.te.missing.filter(e => e.checked)

		this.te.checked = this.te.entries.filter(e => e.checked)

		return this.te
	}

	static getValid() {
		let entries = this.load()
		// filter off same
		entries = te.all.filter(e => e.ru_RU != e.en)
		return valid = entries.filter(e => e.c && (!e.c.langUid || e.c.langUid == e.langUid) && e.en == e.c.en_US)
	}

	get prefix() {
		return this.checked ? '` ' : '`` '
	}

	get suffix() {
		return this.checked ? '\n⟪fromGTable⟫' : '\n⟪fromGTable|notChecked⟫'
	}

	get ru() {
		return this.ru_N + this.suffix
	}

	get ru_apply() {
		return this.prefix + this.ru_RU
	}

	get ru_editor() {
		return this.ru_N + '\n⟪fromGTable⟫'
	}

	constructor(row, fname) {
		this.langUid = +row[0]
		if (fname == 'LANG')
			this.langUid = ''
		this.en = row[1]
		this.ru_RU = row[2]
		this.checked = !!+row[4]
		this.ru_N = this.ru_RU.replace(/\\[a-z](\[[^\]]+\])?/g, '№').trim()

		this.simple = this.ru_N == this.en.replace(/\\[a-z](\[[^\]]+\])?/g, '№').trim()
		this.source = `${fname}.tsv`

		if (fname != 'other' && fname != 'yatl') {
			// this one is for 1-file tables
			this.path = row[3].replace(/"/g, '').split('.').slice(1).join('.').replace('de_DE.', '')
			this.cf = CrossFile.at('data/' + fname)
			this.c = this.cf.infoMap.get(this.path)
			if (this.c)
				this.c.tableNode = this
		} else {
			let a = row[3].replace(/"/g, '').split('.')
			let fname = 'data'
			while (!TableEntry.fileMap.has(fname) && a.length) {
				fname += '/' + a.shift()
			}
			this.cf = TableEntry.fileMap.get(fname)
			if (!this.cf)
				return
			this.cf.generateEnRus()
			this.path = a.join('.')
			this.c = this.cf.infoMap.get(this.path)
			if (!this.c)
				return
			this.c = TableEntry.notaMap.get(this.c.uniq)
			if (this.c)
				this.c.tableNode = this
		}

		this.bots = this.c && this.c.etc && this.c.etc.rus.find(e => e.user == 'DimavasBot')
	}

	notaTlSave() {
		if (this.bots)
			return NotaArea.makeNotaTlEdit(this.bots.href, this.bots.id, this.ru)
		else
			return NotaArea.makeNotaTlAdd(this.c.href, this.ru);
	}
	notaTlRemove() {
		return NotaArea.makeNotaTlRemove(this.bots.href, this.bots.id)
	}

	apply(time) {
		if (!this.simple) {
			let node = this.cf.getNode(this.path)
			if (node.de_DE == this.ru_apply)
				return
			node.de_DE = this.ru_apply
			this.cf.__modified = time
		}
	}
}