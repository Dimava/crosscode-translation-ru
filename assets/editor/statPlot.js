statPlot = {
	PlotlyRequested: false,
	PlotlyPromise: Promise.empty(),
	async requestPlotly() {
		if (!window.Plotly && !this.PlotlyRequested) {
			this.PlotlyRequested = true
			elm('script[src=./lib/plotly-latest.min.js]', load => this.PlotlyPromise.r()).appendTo(document.head)
		}
		return this.PlotlyPromise.p
	},
	clear() {
		this.statPlot && Plotly.purge(this.statPlot)
	},
	async createPlot(data, layout) {
		this.data = data
		this.layout = layout
		let div = elm('#statPlot')
		await this.requestPlotly()

		if (this.newWindow) {
			let o = this.o = open()
			await Promise.wait(100)
			o.document.body.append(div)
		} else {
			if (q('#statPlot')) {
				div = q('#statPlot')
				Plotly.purge(statPlot.plot)
			} else
				cl.append(div, elm('br'))
		}

		Plotly.newPlot(div, data, layout, {
			showSendToCloud: true
		}).then(p => this.plot = p)
		q('#statPlot').scrollIntoView()

	},
	logStatPlot(flags) {

		function intToDate(int) {
			let s = (int + '').replace(/(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/, '20$1-$2-$3 $4:$5:00')
			return s
		}

		function alphaColor(name, alpha) {
			let ctx = document.createElement('canvas').getContext('2d')
			ctx.fillStyle = name
			let n = ctx.fillStyle.match(/\w\w/g).map(e => parseInt(e, 16))
			log(n)
			return `rgba(${n.join(',')},${alpha})`
		}

		let rr = nn.flatMap(n => n.etc.rus)
		let simple = rr.map(({ time, user }) => ({
			time: intToDate(time),
			user: user == 'Cawa00' ? 'Sandorik' : user
		})).filter(e => e.user != 'DimavasBot')
		let usernames = Array.from(new Set(simple.map(e => e.user)))
		let byUser = Object.fromEntries(usernames.map(k => [k, simple.filter(e => e.user == k).map(e => e.time)]).filter(e => e[1].length > 20))

		let traces = Object.entries(byUser).map(([k, v]) => ({
			x: v,
			name: k,
			type: 'histogram',
			cumulative: flags.includes('c') && {
				enabled: true
			},
			xbins: {
				size: 24 * 3600e3,
				start: "2019-10-13",
				//         end: "2020-01-01"
				end: new Date(Date.now() + 3600e3 * 24 * 2).toISOString().replace(/.*?(\d\d)-(\d\d)-(\d\d).*/, '20$1-$2-$3')
			},
			marker: {
				color: this.colors[k]
			}
		}))

		let data = [...traces]
		let layout = {
			barmode: "stack",
		}
		return this.createPlot(data, layout)
	},

	colors: {
		Rik_Zun: 'pink',
		p_zombie: 'green',
		Dimava: 'goldenrod',
		Cilez: 'orangered',
		Rainbow_Spike: 'gray',
		Sandorik: 'lightblue',
		MystAxe: 'seagreen',

		Packy: 'orange',

		Dustex: 'deepskyblue',

		none: '#73828f',
		simple: '#eee',
		GTable: 'seablue',
		GTable_bad: 'blue',

		DimavasBot: 'yellow',
	},
	listToCount(list) {
		let values = Array.from(new Set(list))
		let o = Object.fromEntries(values.map(e => [e, 0]))
		list.map(e => o[e]++)
		return o
	},

	tlQuality(n) {
		if (n.simple)
			return 'simple'
		if (!n.etc.rus.length)
			return 'none'
		if (n.etc.rus.length == 1) {
			let ru = n.etc.rus[0]
			if (ru.user == 'DimavasBot') {
				if (ru.notChecked)
					return 'GTable_bad'
				if (ru.fromGTable)
					return 'GTable'
				return 'DimavasBot'
			}
			if (ru.user == 'Cawa00')
				return 'Sandorik'
			return ru.user
		}
		return n.etc.rus[0].user
	},

	makeFileData(index) {
		let n = NotaArea.areas[index]
		let c = this.listToCount(n.notas.map(this.tlQuality))
		// Object.keys(c).filter(e => c[e] < n.notas.length / 100).map(e => delete c[e])
		let data = {
			values: Object.values(c),
			labels: Object.keys(c),
			hole: 0.6,
			type: 'pie',
			name: n.area,

			title: n.area.replace(/-/g, '<br>'),

			marker: {
				colors: Object.keys(c).map(e => this.colors[e])
			},

			domain: {
				row: Math.floor(index / 6),
				column: index % 6,
			},

			textposition: 'inside',
			textinfo: 'value',
		}
		return data
	},

	logFilePlot() {
		let data = NotaArea.areas.slice(0, 30).map((e, i) => this.makeFileData(i))
		let layout = {
			//     annotations: NotaArea.areas.map(e=>({
			//         text: e.area
			//     })),
			grid: {
				rows: 5,
				columns: 6
			},
			height: 800,
		}

		return this.createPlot(data, layout)
	},

	logSunburstPlot(roots = 1, mfd = e => this.makeFileData(e)) {
		let iits = []
		for (let i = 0; i < 30; i++) {

			let ar = NotaArea.areas[i]
			let fd = mfd(i)

			let its = fd.labels.map((e, i) => ({
				user: e,
				area: fd.name,
				value: fd.values[i],
				color: statPlot.colors[e],
			}))
			its.map(e => e.id = `${e.area} - ${e.user}`)
			its.map(e => e.label = e.user)
			its.map(e => e.parent = e.area)
			let it = {
				type: 'area',
				id: fd.name,
				label: fd.title,
				// value: fd.values.reduce((v, e) => v + e) * (0) + (0) * its.find(e => e.user == 'none').value,
				parent: i >= 2 && i <= 11 ? 'top' : roots > 1 ? 'other' : 'top'
			}
			// its = its.filter(e=>e.user != 'none')

			its.unshift(it)
			its
			iits.push(its)
		}
		iits.unshift([{
			id: 'top',
			label: 'files'
		}])
		if (roots > 1)
			iits.unshift([{
				id: 'other',
				label: 'secondary'
			}])
		let its = iits.flat()

		let data = [{
			type: 'sunburst',
			ids: its.map(e => e.id),
			labels: its.map(e => e.label + (!e.value ? '' : '<br>' + e.value)),
			parents: its.map(e => e.parent || ''),
			values: its.map(e => e.value || 0),
			outsidetextfont: {
				size: 20,
				color: "#377eb8"
			},
			// leaf: {opacity: 0.4},
			marker: {
				line: {
					width: 2
				},
				colors: its.map(e => e.color),
			},
			//   "branchvalues": 'total'
		}]
		let layout = {
			margin: {
				l: 0,
				r: 0,
				b: 0,
				t: 0
			},
			sunburstcolorway: ["#636efa", "#ef553b", "#00cc96"],
		}
		return this.createPlot(data, layout)
	},

	logMissingPlot(roots = 1) {
		function makeFileData(index) {
			n = NotaArea.areas[index]
			c = statPlot.listToCount(n.notas.filter(e => statPlot.tlQuality(e) == 'none').map(e => e.file.split(n.area).pop()))

			data = {
				values: Object.values(c),
				labels: Object.keys(c),
				name: n.area,
				title: n.area.replace(/-/g, '<br>'),
			}
			return data
		}

		return this.logSunburstPlot(roots, makeFileData)
	},

	makeData1(i) {
		return this.listToCount(NotaArea.areas[i].notas.map(this.tlQuality))
	},

	logHBarPlot() {
		let a = NotaArea.areas.map((e, i) => this.makeData1(i))
		let o = Object.assign({}, ...a)
		Object.keys(o).map(k => o[k] = a.reduce((v, e) => v + (e[k] || 0), 0))
		o.simple = -1
		o.none = -2

		let kk = Object.keys(o).sort((a, b) => o[b] - o[a])

		a.map(e => e.summ = Object.values(e).reduce((v, e) => v + e, 0))
		a.reduce((v, e) => e.off = v + e.summ + 50, 0)
		a.map(e => e.off = -e.off)

		let traces = kk.map((user, i) => ({
			x: a.map(e => e[user] || 0).map((e, i) => e / a[i].summ),
			y: a.map(e => e.off),
			offset: 0,
			width: a.map(e => e.summ),
			name: user,
			marker: {
				color: this.colors[user],
			},
			orientation: 'h',
			type: 'bar',
		}))

		let layout = {
			title: 'Colored Bar Chart',
			barmode: 'stack',
			yaxis: {
				tickmode: 'array',
				tickvals: a.map(e => e.off),
				ticktext: NotaArea.areas.map((e, i) => e.area),
			},
			height: 1000,
		}

		return this.createPlot(traces, layout)
	}

}