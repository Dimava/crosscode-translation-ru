updateSelf = {
	async checkForUpdates() {
		// check updates
		let r = await fetch('https://gitlab.com/api/v4/projects/14834525')
		let j = await r.json()

		console.log('upstream version: ', new Date(this.upstreamDate = j.last_activity_at))
		console.log('   local version: ', new Date(this.localDate = localStorage.getItem('updateSelfDate')))

		if (localStorage.getItem('justUpdated')) await this.update(true)

		if (localStorage.getItem('updateSelfDisabled')) return false
		return localStorage.getItem('updateSelfDate') != j.last_activity_at
	},
	async update(force = false) {
		try {
			// check updates
			let r = await fetch('https://gitlab.com/api/v4/projects/14834525')
			let j = await r.json()
			log(j)

			log(localStorage.getItem('updateSelfDate'))
			log(j.last_activity_at)

			if (!force && localStorage.getItem('updateSelfDate') == j.last_activity_at)
				return
			
			if (localStorage.getItem('updateSelfDate') != j.last_activity_at) {
				// load zip
				logs('Загружаем обновление...')
				r = await fetch('https://gitlab.com/api/v4/projects/14834525/repository/archive.zip')
				let a = await r.clone().arrayBuffer()
				fs.ensureSync('./temp/update.zip')
				fs.writeFileSync('./temp/update.zip', new Buffer(a))
			}

			logs('Распаковываем обновление...')
			let AdmZip = require('./lib/adm-zip')
			let zip = new AdmZip('./temp/update.zip')

			// find changed
			let floc = e => './' + e.entryName.split('/').slice(1).join('/')
			let fdir = e => './' + e.entryName.split('/').slice(1, -1).join('/')
			let fsame = e => fs.existsSync(floc(e)) && (zip.readAsText(e) == fs.readFileSync(floc(e)) + '')
			let changed = zip.getEntries().filter(e => !e.isDirectory).filter(e => !fsame(e))

			console.log('changed:', changed.map(floc))
			
			if (!changed.length) {
				localStorage.removeItem('justUpdated')
				return false
			}

			logs('Обновлённые файлы:', ...changed.map(floc))

			// extract
			changed.map(e => zip.extractEntryTo(e, fdir(e), false, true))

			// save date
			localStorage.setItem('updateSelfDate', j.last_activity_at)

			// restart app
			localStorage.setItem('justUpdated', true)
			logs('Перезапускаем программу...')
			chrome.runtime.reload()

		} catch (e) {
			alert(e)
		}
	}
}