q('body').innerHTML = ''
Array(5).fill().ppmap(e => e)

cl = elm('.console').appendTo('body')
cl.style.fontFamily = 'monospace'

function logs(...s) {
	s = s.flatMap(e => e instanceof Node ? e : (e + '').split('\n').flatMap(e => [elm('br'), e]).slice(1))
	cl.append(...s, elm('br'))
}

CrossFile.keepLangs = true

function logb(s, title, flags, ...etc) {
	let b = elm(`button[title=${title}]`, s, ...etc)
	if (flags && flags.includes('d')) {
		if (flags.includes('r')) {
			b.addEventListener('click', () => {
				let p = b
				while (p.previousElementSibling && p.previousElementSibling.matches('button')) {
					p = p.previousElementSibling
				}
				while (p && p.matches('button')) {
					p.disabled = true
					p = p.nextElementSibling
				}
			})
		} else {
			b.addEventListener('click', () => b.disabled = true)
		}

	}
	cl.append(b, ' ')
	if (flags && flags.includes('l'))
		cl.append(elm('br'))
	return b
}

function logtick(id, s, title, flags) {
	let l = elm(`label[for=${id}][title=${title}]`, elm(`input[type=checkbox]#${id}[title=${title}]`), s)
	cl.append(l, ' ')
	if (flags && flags.includes('l'))
		cl.append(elm('br'))
	return l
}

// console.log = logs
function shouldReload(v, done = false, reset = false) {
	if (reset) {
		localStorage['editorReload.' + v] = undefined
	}
	if (shouldReload.reload[v] == localStorage['editorReload.' + v])
		return false
	if (done) {
		localStorage['editorReload.' + v] = shouldReload.reload[v]
	}
	return true
}
shouldReload.reload = {
	replaceDeutschWithEnglish: 8,
	NotaArea: 8,
	apply: 8,
}

buttonActions = {
	async loadFiles() {
		if (!fs.existsSync(`${loc0}/replaceDeutschWithEnglish`) || shouldReload('replaceDeutschWithEnglish')) {
			logs('Ищем файлы...')
			await Promise.frame()
			CrossFile.mergeLang().save()
			await Promise.frame()
			CrossFile.findFiles(shouldReload('replaceDeutschWithEnglish'))

			logs('Ищем файлы с текстом...')
			await CrossFile.findTextFilesAsync(shouldReload('replaceDeutschWithEnglish'))

			logs('Удаляем немецкий...')
			await CrossFile.findTextFiles().pwmap(e => CrossFile.at(e).load().saveBackup().replaceDeutschWithEnglish().save())
			CrossFile.mergeLang()
			CrossFile.at('data/LANG').save()
			CrossFile.splitLang().map(e => e.save())

			fs.writeFileSync(`${loc0}/replaceDeutschWithEnglish`, 'done')
			shouldReload('replaceDeutschWithEnglish', 1)
		}

		logs('Открываем файлы...')
		await CrossFile.findTextFiles().ppmap(e => CrossFile.at(e).loadAsync(), 50)
	},
	async loadNota() {
		if (!NotaArea.mapLinks) {
			logs('Загружаем ноту...')
			await NotaArea.fetchMapLinks()
			if (!NotaArea.areas.length) {
				logs('Вход не произведен')
				logs('Ожидание входа...')
				w = open('http://notabenoid.org/book/74823')
				logb('Я зашел', 'Если нет акка, потыкай @Dimava', 'dr').onclick = () => w.close() + location.reload()
				logb('Я зомби', 'Войти без аккаунта', 'ldr').onclick = buttonActions.imZombie
				buttonActions.zombie_p = Promise.empty()
				await buttonActions.zombie_p.p
				await NotaArea.fetchMapLinks(true)
			}
			logs('Вошёл как ' + NotaArea.user)
			await NotaArea.prefetch(shouldReload('NotaArea'))
		}

	},

	async imZombie() {
		w.close()
		NotaArea.zombie = true
		buttonActions.zombie_p.r()
	},

	async reloadNotaData() {
		logs('Перевод перекачивается')
		shouldReload('NotaArea', false, true)
		chrome.runtime.reload()

		await NotaArea.prefetch(true)
		await NotaArea.areas.ppmap(async (n, i) => {
			await Promise.wait(20, i)
			await n.fetchArea(1)
			n.generateEnRus()
		}, 999)

		ff = CrossFile.findTextFiles().map(e => CrossFile.at(e))
		ff.map(e => e.load(true).generateEnRus())

		cc = ff.flatMap(e => e.infos).filter(e => e.index)
		nn = NotaArea.areas.flatMap(e => e.notas)
		nn.map(n => n.etc.rus.map(r => r.nota = n))
		rr = nn.filter(e => e.index).flatMap(e => e.etc.rus)
		logs('Перевод был перекачан')
		return this.applyNotaData()
	},
	async fulfillNotaData() {
		logs('Перевод обновляется...')

		ff = CrossFile.findTextFiles().map(e => CrossFile.at(e))
		ff.map(e => e.load().generateEnRus())
		cc = ff.flatMap(e => e.infos).filter(e => e.index)
		await NotaArea.areas.ppmap(async (n, i) => {
			await n.fetchArea(shouldReload('NotaArea'))
			n.generateEnRus()
		}, 1)
		nn = NotaArea.areas.flatMap(e => e.notas)
		nn.map(n => n.etc.rus.map(r => r.nota = n))
		rr = nn.filter(e => e.index).flatMap(e => e.etc.rus)
		shouldReload('NotaArea', 1)
	},
	async applyNotaData() {
		logs('Строки применяются...')
		await NotaArea.areas.flatMap(e => e.notas).pwmap(e => e.apply(this.rnd), 500)
	},

	async applyTable() {
		logs('Из таблиц дополняются...')
		return await TableEntry.load().missing.pwmap(e => e.apply(), 500)
	},

	async loadBackup() {
		logs('Файлы загружаются из бекапа...')
		let fnames = ['map-content', 'gui', 'gimmick'].map(e => `data/lang/sc/${e}.de_DE`).concat(CrossFile.findTextFiles())
		await fnames.pwmap(e => CrossFile.at(e).loadBackup().replaceDeutschWithEnglish(), 10)
		// CrossFile.getFiles.map(e=>e.__modified = -1)
	},
	async save() {
		logs('Перевод сохраняется...')
		await CrossFile.saveAllAsync(true)
		logs('Перевод применен к папке игры')
	},
	async export () {
		logs('Перевод экспортируется...')
		fs.ensureSync(this.rndloc)
		fs.copySync('./assets/editor/tl-src', this.rndbaseloc)
		await CrossFile.getFiles().ppmap(e => e.saveAtAsync(this.rndloc), 50)
		CrossFile.splitLang().map(e => e.saveAt(this.rndloc))
		logs(`Перевод сохранен в папке ${fs.realpathSync(this.rndloc)}.`)
		require('child_process').exec(`start "" "${fs.realpathSync(this.rndbaseloc)}"`);
	},

	async apply(backup = false, table = false, exprt = false) {

		this.rnd = new Date().toISOString().slice(0, 16).replace(':', '') // Date.now()
		this.rndbaseloc = `./temp/CrossCode RUS v1.2.0-4 b${this.rnd}${table?' YandexTlFallback':' EnFallback'}`
		this.rndloc = this.rndbaseloc + '/assets'

		if (fs.existsSync('./readme.txt')) {
			logs('Обнаружен readme.txt')
			logs('Предполагается восстановление из старого билда, строки не применяются.')
			logb('Вернуться к новейшей версии перевода', 'Удалит readme.txt и переприменит перевод', 'ldr', click => {
				shouldReload('apply', 1, 1)
				fs.unlinkSync('./readme.txt')
				chrome.runtime.reload()
			})
			logs('')
			CrossFile.recoveryMode = true
			NotaArea.recoveryMode = true
			return
		}

		log(this.rnd)

		if (exprt) CrossFile.keepLangs = false

		if (backup || shouldReload('apply')) await this.loadBackup()
		CrossFile.mergeLang()

		await this.applyNotaData()
		if (table) await this.applyTable()

		await this.save()
		if (exprt) await this.export()

		shouldReload('apply', 1)
	},

	restartApp(type = '') {
		if (!localStorage.getItem('autoLaunch'))
			localStorage.setItem('autoLaunch', 'once')
		localStorage.setItem('autoLaunch_type', type)
		chrome.runtime.reload()
	},

	clearUI(type) {
		if (type == 'GridUI') {
			TableEntry.load()
			ui = new GridUI().makeUI(true)
			return ui
		}
	},

	applyMods() {
		let s0 = fs.readFileSync(`${loc0}/js/game.compiled.js`, 'utf-8')
		let s = s0
		s = s.replace(/this\.special\.setPos\(7,\s*9\)/, 'this.special.setPos(0,0)')

		let g1 = /this.location\s*=\s*new sc.TextGui\(\(b.area[^]{0,999}?this.addChildGui\(this.location2?\);?\s*\}/g

		let r1 = `
					this.location = new sc.TextGui((b.area || "???"), { font: sc.fontsystem.smallFont });
					this.location.setPos(50, 19);
					this.location2 = new sc.TextGui('  > ' + (b.map || "???"), { font: sc.fontsystem.smallFont });
					this.location2.setPos(50, 19 + 10);
					// this.location2.setAlign(ig.GUI_ALIGN.X_RIGHT, ig.GUI_ALIGN.Y_TOP);
					this.addChildGui(this.location);
					this.addChildGui(this.location2)
				}
		`
		s = s.replace(g1, r1)

		let g2 = /this\._largeWidth\s*=\s*d\s*\|\|\s*106;([^;]+;;)?/
		let r2 = `
			this._largeWidth = d || 106;
			if (window.pxlen) this._largeWidth = Math.max(this._largeWidth, window.pxlen(b + '') + 30);;
		`
		s = s.replace(g2, r2)

		let g3 = /(this.endDescriptionGui\s*=\s*new[^;]*;)([^;]*;;)?/
		let r3 = `$1 window.ontext && window.ontext(this.quest.endDescription);;`
		s = s.replace(g3, r3)

		let g4 = /(this.firstTaskGui.doStateTransition[^}]*\})([^;]*;;)?/
		let r4 = `$1 window.ontext && window.ontext(this.quest.description)+window.ontext && window.ontext(this.quest.name);;`
		s = s.replace(g4, r4)

		let g5 = /(this.tracker\s*&&[^;]*;)([^;]*;;)?/
		let r5 = `$1 window.ontext && window.ontext(this.text);;`
		s = s.replace(g5, r5)

		let g6 = /(g\s*=\s*h.quest.tasks[^;]*;)([^;]*;;)?/
		let r6 = `$1 window.ontext && window.ontext(g.task);;`
		s = s.replace(g6, r6)

		let g7 = /(this.parent\(134,\s*20\);)([^;]*;;)?/
		let r7 = `$1 this.__content = b;;`
		s = s.replace(g7, r7)

		let g8 = /(return\s*b\s*==\s*sc.INTERACT_ENTRY_STATE.FOCUS)([^}]*)/
		let r8 = `$1 && (window.ontext && window.ontext(this.__content), true);;`
		s = s.replace(g8, r8)

		if (s != s0) {
			fs.writeFileSync(`${loc0}/js/game.compiled.js`, s)
		}
	},

	async alignWindows() {
		await Promise.frame(10)

		return;

		let nww1 = nw.Window.get(window)
		let nww2 = nw.Window.get(w)

		if (!nww1.appWindow || !nww2.appWindow) return
		if (nw.Screen.screens.length > 1) return
		let sb = nw.Screen.screens[0].bounds
		if (sb.height != 1080 || sb.width != 1920) return

		let ib = nww2.appWindow.innerBounds
		if (ib.height != 640) return
		ib.left = sb.width - ib.width
		ib.top = sb.height / 2 - ib.height / 2

		await Promise.frame(10)

		let aw1 = nww1.appWindow

		aw1.maximize()

		await Promise.frame(10)

		aw1.innerBounds.width = ib.left
	},

	async startGame(onlyGrid = false) {
		this.clearUI('GridUI')
		if (window.w && window.w.close) window.w.close()
		if (onlyGrid) {
			await ui.init()
			return
		}
		w = open('/assets/node-webkit.html')
		await Promise.wait(100)
		await this.alignWindows()
		await ui.init(w)
	},

	async restartGame(refresh = true, reset = true) {
		if (window.w && window.w.close) window.w.close()
		if (reset) q('body').innerHTML = ''
		if (refresh) {
			cl = elm('.console').appendTo('body')
			cl.style.fontFamily = 'monospace'
			if (reset) NotaArea.reset()
			await this.loadNota()
			await this.fulfillNotaData()
			await this.apply()
		}
		this.startGame()
	},

	async addAutoBtn() {
		this.autoLaunchWait = Promise.empty()
		updateSelf.checkForUpdates().then(updated => {
			if (updated) {
				logs('Найдено обновление!')
				logb('Обновить!', 'Скачать и распаковать обновление', 'ldr', click => updateSelf.update())
				if (confirm(`Найдено обновление переводилки\nТекущая версия: ${updateSelf.localDate}\nНовая версия:     ${updateSelf.upstreamDate}\nОбновить?`)) {
					updateSelf.update()
				}
			}
		})

		let autoLaunch = localStorage.getItem('autoLaunch')
		if (!autoLaunch) {
			logb('Запустить как только будет готово', 'Игра и редактор будут запущены как только так сразу.', 'ldr', click => buttonActions.autoLaunch())
			return
		}
		if (autoLaunch == 'once') {
			localStorage.removeItem('autoLaunch')
		}
		let autoLaunch_type = localStorage.getItem('autoLaunch_type')
		localStorage.removeItem('autoLaunch_type')
		this.autoLaunch(autoLaunch_type == 'onlyGrid')
	},

	async autoLaunch(onlyGrid = false) {
		logs('Игра и редактор будут запущены как только так сразу.')
		await this.autoLaunchWait.p
		await Promise.wait(1000)
		this.startGame(onlyGrid)
	},

	async requestPlot() {
		statPlot.requestPlotly()
		elm('label', elm('input[type=checkbox]', input => statPlot.newWindow = input.target.checked), 'new win').appendTo(cl)
		logb('histogram', '', '', click => statPlot.logStatPlot('h'))
		logb('cumulative', '', '', click => statPlot.logStatPlot('hc'))
		logb('locations', '', '', click => statPlot.logFilePlot())
		logb('location-bars', '', '', click => statPlot.logHBarPlot())
		logb('sun', '', '', click => statPlot.logSunburstPlot(1))
		logb('sun2', '', '', click => statPlot.logSunburstPlot(2))
		logb('missing', '', '', click => statPlot.logMissingPlot(1))
		logb('missing2', '', '', click => statPlot.logMissingPlot(2))
	},

	async addStyling() {
		logs('Переключить стили: ',
			elm('label.PackyStyle', elm('input[type=checkbox]', change => q('body').classList.toggle('PackyStyle')), 'PackyStyle')
		)
	},

	p: null,
	resetP() {
		this.p = Promise.empty()
		this.p.p.then(() => this.resetP())
	}
}
buttonActions.resetP();

void async function() {
	try {
		await buttonActions.addAutoBtn()

		buttonActions.addStyling()

		await buttonActions.loadFiles()

		await buttonActions.loadNota()

		await buttonActions.fulfillNotaData()
		await buttonActions.apply()
		buttonActions.p.r()

		logb('Перекачать перевод с ноты', 'ВЕСЬ перевод с ноты будет заново перекачан.', 'dr', click => buttonActions.reloadNotaData())
		// logb('Применить обновлённый перевод с ноты', 'Будет применена та часть перевода, которая изменилась.', 'dr', click => buttonActions.dl(2))
		// logb('Просто запустить игру', 'Игра будет запущена, редактор не появится.', 'dr', click => open('/assets/node-webkit.html') + close())
		logb('График', 'Вывести статистику.', 'd', click => buttonActions.requestPlot())
		logb('JS-моды', 'Применить модификации к js игры.', 'ld', click => buttonActions.applyMods())

		logtick('tick-backup', 'Чистая установка', 'Восстановится английский язык, и поверх него будет загружен перевод.')
		logtick('tick-table', 'Гугл таблицы', 'Для строк без перевода на ноте будет взят перевод из гуглтаблиц, если есть.')
		logtick('tick-export', 'Экспортировать', 'Перевод будет сохранен в папку CrossCode/temp/.')

		logb('Применить', 'Применить перевод к файлам игры', 'lr', //
			click => buttonActions.apply(q('#tick-backup').checked, q('#tick-table').checked, q('#tick-export').checked) //
		)

		logb('Запустить игру', 'Будут открыты игра и редактор', 'dr', click => buttonActions.startGame())

		logb('Запустить только переводилку', 'Будет открыт редактор', 'drl', click => buttonActions.clearUI('GridUI').init())

		buttonActions.autoLaunchWait.r()

	} catch (e) {
		logs(lastErr = e, e.stack)
		throw e
	}

}()
