NotaEtc = class NotaEtc {
	constructor(etc) {
		Object.assign(this, etc)
		this.rus = this.rus.map(e => new NotaRu(e))
	}
	static from(etc) {
		etc.rus = etc.rus.map(e => NotaRu.from(e))
		etc.rus = etc.rus.sort((a, b) => b.rating - a.rating)
		etc.ru = etc.rus.length == 0 ? '' : etc.rus[0].ru
		Object.setPrototypeOf(etc, NotaEtc.prototype)
		return etc
	}

	async translate(ru, flags) {
		let res = await NotaArea.makeNotaTlAdd(this.href, ru, flags)
		let e = elm()
		e.innerHTML = res.text
		let div = e.children[e.childElementCount - 1]
		let oru = NotaArea.parseRu(div, this)
		this.rus.push(oru)
		Object.assign(this, NotaRu.from(this))
		return oru
	}
}

NotaRu = class NotaRu {
	constructor(e) {
		Object.assign()
	}
	static from(ru) {
		Object.setPrototypeOf(ru, NotaRu.prototype)
		return ru
	}

	remove() {
		return NotaArea.makeNotaTlRemove(this.href, this.id)
	}
	change(ru = this.raw_ru, flags = this.flags) {
		return NotaArea.makeNotaTlEdit(this.href, this.id, ru, flags)
	}
}

NotaArea = class NotaArea {
	static makeNotaEdit(url, ord, body) {
		url = url.split('#')[0] + '/edit'
		let fd = new FormData()
		fd.append('Orig[ord]', ord)
		fd.append('Orig[body]', body)
		fd.append('ajax', 1)
		return fetch(url, {
			method: 'POST',
			body: fd,
			credentials: 'include',
		}).then(e => e.json())
	}
	static makeNotaRemove(url, ...etc) {
		if (etc.length)
			throw 'too much args'
		url = url.split('#')[0] + '/remove'
		return fetch(url, {
			credentials: 'include'
		}).then(e => e.json())
	}
	static makeNotaTlEdit(url, id, ru_RU, flags) {
		// edit   =  Translation[body]: Вход & ajax: 1
		// http://notabenoid.org/book/74823/421327/129206670/translate?tr_id=74913365 
		url = url.split('#')[0] + '/translate?tr_id=' + id
		let fd = new FormData()
		fd.append('Translation[body]', this.applyFlags(ru_RU, flags))
		fd.append('ajax', 1)
		return fetch(url, {
			method: 'POST',
			body: fd,
			credentials: 'include',
		}).then(e => e.json())
	}
	static makeNotaTlAdd(url, ru_RU, flags) {
		// http://notabenoid.org/book/74823/452147/138414139/translate
		// Translation[body]: 123
		// ajax: 1
		url = url.split('#')[0] + '/translate'
		let fd = new FormData()
		fd.append('Translation[body]', this.applyFlags(ru_RU, flags))
		fd.append('ajax', 1)
		return fetch(url, {
			method: 'POST',
			body: fd,
			credentials: 'include',
		}).then(e => e.json())
	}
	static makeNotaTlRemove(url, id) {
		// http://notabenoid.org/book/74823/421327/129206669/tr_rm  & tr_id: 72588648
		url = url.split('#')[0] + '/tr_rm'
		let fd = new FormData()
		fd.append('tr_id', id)
		return fetch(url, {
			method: 'POST',
			body: fd,
			credentials: 'include',
		}).then(e => e.json())
	}
	static makeNotaTlVote(url, id, value) {
		// http://notabenoid.org/book/74823/421327/129206669/tr_rm  & tr_id: 72588648
		url = url.split('#')[0].split('/').slice(0, -1).join('/') + '/rate_tr'
		let fd = new FormData()
		fd.append('id', id)
		fd.append('mark', value)
		return fetch(url, {
			method: 'POST',
			body: fd,
			credentials: 'include',
		}).then(e => e.json())
	}

	static async makeNotaRelogin(login, pass) {
		await fetch('http://notabenoid.org/register/logout', {
			credentials: 'include',
		})
		let url = 'http://notabenoid.org/'
		let fd = new FormData()
		fd.append('login[login]', 'DimavasBot')
		fd.append('login[pass]', localStorage.getItem('DimavasBotPassword'))
		fd.append('ajax', 1)
		fetch(url, {
			method: 'POST',
			body: fd,
			credentials: 'include',
		}).then(e => console.log('relogged'))
	}

	static parseRu(e, etc) {
		let ru = {
			raw_ru: e.q('.text').innerText,
			user: e.q('.user').innerText,
			votes: +e.q('.rating .current').innerText,
			best: e.matches('.best'),
			id: +e.id.slice(1),
			time: +e.q('.info').childNodes[3].nodeValue.replace(/\D*(\d+)\D*(\d+)\D*(\d+)\D*(\d+)\D*(\d+)\D*/g, (s, a, b, c, d, e) => [c, b, a, d, e].map(e => `00${e}`.slice(-2)).join(''))
		}

		ru.flags = NotaArea.getFlags(ru.raw_ru)
		for (let f in ru.flags) {
			if (f.length >= 10)
				ru[f] = ru.flags[f]
		}
		if (ru.raw_ru.replace(/\n?⟪.*⟫/, '') != ru.raw_ru.replace(/\n?⟪.*⟫/, '').trim()) {
			console.warnGroup('invalid translation: has whitespace on ends', etc)
			ru.err += ' err_not_trimmed '
			etc.err_not_trimmed = true
		}
		ru.ru = NotaArea.applyFlags(ru.raw_ru, false)
		ru.ru = ru.ru.replace(/^\^|^\$|\$$/g, '')

		ru.err = '';
		if (ru.ru.startsWith(etc.en.split(' ')[0]) && !ru.ru.startsWith('---')) {
			if (ru.ru.match(/\n\n/))
				ru.ru = ru.ru.split('\n\n')[1]
			else if (ru.ru.match(/^[\x00-\xff\s]*\n([^]+?)\s*$/))
				ru.ru = ru.ru.match(/^[\x00-\xff\s]*\n([^]+?)\s*$/)[1]
			console.warnGroup('invalid translation: has path', etc)
			ru.err += ' err_has_path '
			etc.err_has_path = true
		}
		if (ru.ru.includes('№')) {
			let enm = NotaArea.getEscapeSequence(etc.en)
			let rum = ru.ru.match(/№/g) || []
			if (enm.length != rum.length) {
				console.warnGroup('invalid translation: № dismatch', etc)
				ru.err += ' err_dismatch '
				etc.err_dismatch = true
			}
			if (NotaArea.getEscapeSequence(ru.ru).length) {
				console.warnGroup('invalid translation: mixed №/\\c escape sequences', etc)
				ru.err += ' err_mixed_escape '
				etc.err_mixed_escape = true
			}
			enm = [...enm]
			ru.ru = NotaArea.applyEscapeSequence(ru.ru, enm)
		}
// 		if (NotaArea.getEscapeSequence(ru.raw_ru).length) {
// 			if (NotaArea.getEscapeSequence(ru.raw_ru) + '' == NotaArea.getEscapeSequence(etc.en) + '') {
// 				console.warnGroup('invalid translation: same escape sequence', etc)
// 				ru.err += ' err_same_escape '
// 				etc.err_same_escape = true
// 			}
// 		}
		if (!NotaArea.checkValidEscape(ru.ru)) {
			console.warnGroup('invalid translation: invalid escape sequence', etc)
			ru.err += ' err_invalid_escape '
			etc.err_invalid_escape = true
		}
		if (!NotaArea.checkValidCharset(ru.ru)) {
			console.warnGroup('invalid translation: non-existing character', etc)
			ru.err += ' err_invalid_charset '
			etc.err_invalid_charset = true
		}
		ru.href = etc.href.split('#')[0]

		ru.rating = 1e10 + 1e10 * ru.votes + ru.time - 19e8
		if (ru.user == 'p_zombie')
			ru.rating -= 1e9
		if (ru.user == 'DimavasBot') {
			ru.rating -= 3e9
			if (ru.fromGTable && !ru.notChecked)
				ru.rating += 1e9
		}
		if (ru.flags.overflow && localStorage.getItem('downvoteOverflow'))
			ru.rating -= 1e11
		return ru
	}

	static parseTr(tr) {
		let divs = tr.qq('.t>div')
		let etc = {
			en: tr.q('.o .text').innerText,
			href: tr.q('.o .ord').href,
		}
		etc.ord = +etc.href.split('#')[1]

		let rus = divs.map(e => NotaArea.parseRu(e, etc))
		etc.rus = rus.sort((a, b) => b.rating - a.rating)

		etc.ru = etc.rus.length == 0 ? '' : etc.rus[0].ru

		return etc
	}

	static get charset() {
		if (this._charset) return this._charset
		let charset = cc.flatMap(e => e.en_US.split('')).unique()
		let ru = 'АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюяЁё⟪|⟫№'
		charset = charset.concat(ru.split(''))
		return this._charset = new Set(charset)
	}

	static checkValidCharset(s) {
		let charset = this.charset
		return !s.split('').filter(e => !charset.has(e)).length
	}

	static get flags() {
		if (this._flags) return this._flags
		let flags = Object.assign({}, ...rr.map(e => e.flags))
		return this._flags = Object.keys(flags)
	}

	static checkValidEscape(s) {
		if (!s.includes('\\'))
			return true
		return !s.match(/\\(?![civs]\[|[.!])/)
	}

	static getEscapeSequence(s) {
		return Array.from(s.match(/\\[civs](\[[^\]]+\])?/g) || [])
	}

	static applyEscapeSequence(s, seq) {
		if (typeof seq == 'string')
			seq = this.getEscapeSequence(seq)
		let i = 0
		return s.replace(/№/g, s => seq[i++])
	}

	static getFlags(s) {
		let m = s.match(/⟪(.*)⟫/)
		if (!m) return {}
		s = m ? m[1] : s
		if (!s) return {}
		let flags = {}
		let strs = s.split('|').map(e => e.trim())
		for (let f of strs) {
			let [k, v] = f.split(':')
			flags[k] = v || true
		}
		return flags
	}

	static applyFlags(s, ...flags) {
		if (flags[0] === false)
			return s.replace(/\n?⟪([^]*)⟫\s*/, '')
		if (!flags[0])
			return s

		flags = flags.map(f => typeof f == 'string' ? this.getFlags(f) : f)
		flags = Object.assign(this.getFlags(s), ...flags)
		s = s.replace(/⟪.*⟫/, '').trim()
		if (!Object.keys(flags).length)
			return s
		flags = Object.entries(flags).map(([k, v]) => v === true ? k : `${k}:${v}`)
		return `${s}\n⟪${ flags.join('|') }⟫`
	}

	static async fetchMapLinks(force = false) {
		if (this.mapLinks && !force)
			return this.mapLinks
		let d0 = await fetch.doc(!NotaArea.zombie ? 'http://notabenoid.org/book/74823' : 'https://opennota2.duckdns.org/book/74823')
		let trs = d0.qq('tr[id]')
		this.mapLinks = Object.fromEntries(trs.map(e => e.q('a')).map(a => [a.innerText, a.href]))
		this.areaStatus = Object.fromEntries(trs.map(e => {
			if (!e.q('span'))
				return [e.q('a').innerText, {
					area: e.q('a').innerText,
					href: e.q('a').href
				}]
			let nn = e.q('span').title.replace(/[а-я]+/, s => ['--', 'янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'].indexOf(s)).match(/\d+/g)
			return [e.q('a').innerText, {
				area: e.q('a').innerText,
				href: e.q('a').href,
				size: +e.q('small').innerText.split(/\D/).filter(Boolean).pop(),
				time: Date.parse(`${nn[2]}/${nn[1]}/${nn[0]} ${nn[3]}:${nn[4]} +0300`)
			}]
		}))
		this.areas = Object.keys(this.mapLinks).map(e => new NotaArea(e))
		if (!this.areas.length)
			return
		this.areaMap = Object.fromEntries(this.areas.map(e => [e.area, e]))

		this.isMod = d0.qq('.info dt').find(e => e.innerText == 'Ваш статус:') && //
			!!d0.qq('.info dt').find(e => e.innerText == 'Ваш статус:').nextElementSibling.innerText.match(/модератор|создатель/)
		this.user = d0.qq('#header-submenu li a strong')[0].innerText

		await this.areas.pwmap(e => CrossFile.generateLocation(e.area) + e.loadArea(), 1)
		return this.mapLinks
	}

	constructor(area) {
		// expects finished fetchMapLinks
		this.area = area
		this.href = NotaArea.mapLinks[area]
		this.size = NotaArea.areaStatus[area].size || 0
		this.time = NotaArea.areaStatus[area].time || 0
	}

	static at(area) {
		return this.areaMap[area]
	}

	loadArea(force = false) {
		if (force)
			return null
		if (this.trList)
			return this.trList

		delete this.infos
		delete this.notaMap

		let fname = loc0 + '/dls/' + this.area + '.etc'
		fs.ensureSync(fname)
		if (force || !fs.existsSync(fname))
			return

		let j = JSON.parse(fs.readFileSync(fname))
		this.updated = j.time != this.time || j.zombie != NotaArea.zombie
		if (this.updated)
			return
		return this.trList = j.trList.map(e => NotaEtc.from(e))
	}

	static async prefetch(force = false) {
		let updated = this.areas.filter(e => e.updated)
		if (force)
			updated = this.areas
		this.docs = this.docs || {}
		let links = []
		for (let area of updated) {
			for (let i = 1; i * 50 <= area.size; i++)
				links.push(`${area.href}?Orig_page=${i}`)
		}
		links = links.filter(e => !this.docs[e])
		console.groupCollapsed(`prefetching ${links.length} nota pages`)
		let r = await links.ppmap(async e => this.docs[e] = this.docs[e] || await fetch.doc(e))
		console.groupEnd()
		this.prefetched = true
		return r
	}

	async fetchArea(force = false) {
		if (this.loadArea(force) && !force)
			return this.trList

		let nextHref = this.href + '?Orig_page=1'
		let trList = []

		console.enterGroup(`invalid translations in ${this.area}`)
		do {
			let doc = NotaArea.docs && NotaArea.docs[nextHref]
			if (doc)
				delete NotaArea.docs[nextHref]
			else
				doc = await fetch.doc(nextHref)

			doc.qq('tr[id]').map(tr => NotaArea.parseTr(tr)).map(e => trList.push(e))

			nextHref = doc.q('[title="На следующую страницу"]') && doc.q('[title="На следующую страницу"]').href
			// nextHref = 0 // TODO
		} while (nextHref) //
		console.exitGroup(`invalid translations in ${this.area}`)

		let fname = loc0 + '/dls/' + this.area + '.etc'
		fs.writeFileSync(fname, JSON.stringify(Object.assign({}, NotaArea.areaStatus[this.area], {
			trList,
			zombie: NotaArea.zombie
		})))
		this.trList = trList.map(e => NotaEtc.from(e))
		this.updated = false
		//.filter(e=>e.ord > 99)
		return this.loadArea()
	}

	generateEnRus(force = false) {
		if (this.infos && !force)
			return this.infos
		if (!this.trList)
			return "not loaded!"
		// expects finished fetchArea
		let infos = CrossFile.generateLocation(this.area)

		let trEnRus = this.trList.map(e => new CrossEnRu().setTr(e))
		// this.trList = null

		let notas = trEnRus.map(e => {
			let c = CrossFile.at(e.file)
			if (!c)
				return e
			c.load().generateEnRus()
			if (!e.path)
				return c.infos[0].setTr(e.etc)
			if (e.path && !c.__text_paths.hasOwnProperty(e.path))
				return e
			return c.infoMap.get(e.path).setTr(e.etc)
		})

		let notaMap = new Map(notas.map(e => [e.uniq, e]))
		this.notaMap = notaMap

		// infos.map(e=>{
		//     notaMap.has(e.uniq) && e.setTr(notaMap.get(e.uniq).cloneFrom(e).etc)
		// }
		// )

		infos.map(e => Object.defineValue(e.crossFile, 'nota', this))

		log({
			nota: this,
			area: this.area,
			infos,
			notas,
		})
		this.infos = infos
		this.notas = notas
		return infos
	}

	static generateEnRusDifference() {
		return {
			no_etc: cc.filter(e => !e.etc),
			no_enru: nn.map(e => e.enruNode().__path).filter(e => !e),
			unordered: nn.filter(e => e.etc && e.ord != e.etc.ord),
			dupes: 'not implemented',
		}
	}

	static async fixOrder() {
		let unordered = this.generateEnRusDifference().unordered
		console.log(unordered)

		unordered.map(e => e.index += 5000)
		await unordered.filter(e => e.ord != e.etc.ord).ppmap(e => e.notaUpload(), 10)
		unordered.map(e => e.index -= 5000)
		await unordered.ppmap(e => e.notaUpload(), 10)
	}

	setProgress(i, a) {
		let p = q('progress') || elm('progress').appendTo('body')
		p.max = a.length
		p.value = i + 1
		let s = q('span') || elm('span').appendTo('body')
		s.innerText = ` ${i + 1} / ${a.length}`
		return true
	}

	applyAndSave(time = Math.random(), loc = './temp/assets') {
		this.notas.map(e => e.apply(time))
		let s = new Set(this.notas.map(e => e.crossFile).filter(Boolean))
		let m = Array.from(s).filter(e => e.__modified == time)
		if (loc) {
			m.map(e => e.saveAt(loc))
		} else {
			m.map(e => e.save())
		}
	}

	static reset() {
		for (let k of Object.keys(this)) {
			delete this[k]
		}
		this.zombie = false
	}

	static fixError_err_same_escape() {
		let a = rr.filter(r => r.err.includes('err_same_escape'))
		log('err_same_escape:', a)
		a.map(e => e.raw_ru = e.raw_ru.replace(/\\[civs](\[[^\]]+\])?/g, '№'))
		a.ppmap(e => e.change())
	}
}

NotaArea.zombie = false
