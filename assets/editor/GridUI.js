GridNode = class GridNode {
	constructor(nota, ui) {
		this.nota = nota
		this.ui = ui
		nota.node = this
		this.removed = true
		this.elem = elm('tr.tl-node')
		this.el_en = elm('.tl-en').appendTo(elm('td').appendTo(this.elem))
		this.el_rus = elm('.tl-rus').appendTo(elm('td').appendTo(this.elem))

		elm('.tl-en-file', nota.file).appendTo(this.el_en)
		elm('.tl-en-path', nota.path).appendTo(this.el_en)

		elm('.tl-en-desc', nota.description).appendTo(this.el_en)

		elm('.tl-en-text', ...this.trimText(nota.enruNode().en_US)).appendTo(this.el_en)

		if (nota.etc) {

			elm(`a.tl-en-link[href=${nota.etc.href}][target=_blank]`, '#' + nota.etc.ord).appendTo(this.el_en)
			let ta = this.textarea_auto_resize('textarea.tl-en-edit[placeholder=новый перевод]', //
				keyup => keyup.ctrlKey && keyup.code == 'Enter' && (keyup.preventDefault() + this.click_add()), //
				focus => this.en_flags.hidden = false, //
			).appendTo(this.el_en)
			this.ta = ta
			elm('button.tl-en-edit[title=Добавить введенный перевод]', '✔', click => this.click_add()).appendTo(this.el_en)

			if (nota.limits) {
				this.en_limits = elm('.tl-en-edit.tl-en-limits').appendTo(this.el_en)
				elm('span.tl-en-limit', `0/0`).appendTo(this.en_limits)
				elm('.tl-en-limit-filler').appendTo(this.en_limits)
				this.textarea_calc_limit(ta.value)
				ta.addEventListener('input', input => this.textarea_calc_limit(ta.value))
			}

			this.en_flags = elm('.tl-en-flags[hidden=true]').appendTo(this.el_en)
			this.flags = {}
			for (let flag of NotaArea.flags) {
				let fel = elm('span.tl-en-flag', flag, click => {
					if (fel.classList.toggle('tl-en-flag-on')) {
						this.flags[flag] = true
					} else {
						delete this.flags[flag]
					}
				}).appendTo(this.en_flags)
			}


			if (!nota.etc.rus.length) {
				this.el_rus.classList.add('tl-no-rus')
			}

			if (NotaArea.recoveryMode) {
				let node = elm('.tl-ru.tl-ru-recovery').appendTo(this.el_rus)
				let text = nota.enruNode().de_DE
				let el_rutext = elm('.tl-ru-text', nota.enruNode().de_DE, dblclick => (ta.value = ta.value || text) + ta.oninput()).appendTo(node)
				if (nota.etc.rus.find(e => e.ru == text))
					node.classList.add('tl-ru-recovery-found')
				else
					node.classList.add('tl-ru-recovery-removed')
			}

			this.els_ru = nota.etc.rus.map(ru => this.elmRu(ru))

			if (!this.els_ru.length) {
				let de = nota.enruNode().de_DE
				let en = nota.enruNode().en_US
				if (de != en) {
					this.el_en.q('textarea').value = de
					this.textarea_calc_limit(de)
				}
			}

			if (nota.tableNode) {
				let tn = nota.tableNode
				let node = elm('.tl-ru.tl-ru-gtable').appendTo(this.el_rus)
				elm('.tl-ru-user', tn.source).appendTo(node)
				elm('.tl-ru-rate' + (tn.checked ? '.tl-ru-upvoted' : ''), tn.checked ? 'checked' : ' ').appendTo(node)
				let el_rutext = elm('.tl-ru-text', tn.ru_N, dblclick => (ta.value = ta.value || tn.ru_N) + ta.oninput()).appendTo(node)
			}
		}

	}

	elmRu(ru) {
		let node = elm('.tl-ru').appendTo(this.el_rus)
		elm('.tl-ru-user', ru.user + (ru.err || '').replace(/\s+/g, '\n')).appendTo(node)

		elm('.tl-ru-rate' + (ru.votes > 0 ? '.tl-ru-upvoted' : '') + `[title=${this.timen_to_str(ru.time)}]`, Math.round(ru.rating / 1e6) / 1e4).appendTo(node)
		let text = this.trimText(NotaArea.applyFlags(ru.raw_ru, false).trim())
		let el_rutext = elm('.tl-ru-text', ...text, dblclick => (this.ta.value = this.ta.value || ru.raw_ru) + this.ta.oninput()).appendTo(node)
		elm('button.tl-ru-minus', '-1', click => this.click_votedown(ru, node)).appendTo(node)
		elm('button.tl-ru-plus', '+1', click => this.click_voteup(ru, node)).appendTo(node)
		elm('button.tl-ru-change', '✎', click => this.click_change(ru, node)).appendTo(node)
		elm('button.tl-ru-remove', '✖', click => this.click_remove(ru, node)).appendTo(node)

		if (ru.err.includes('err_invalid_charset')) {
			el_rutext.innerHTML = el_rutext.innerHTML.replace(/&.*?;|(.)/g,
				(s, a) => !a ? s : NotaArea.charset.has(a) ? a : `<span style="background:red;">${a}</span>`)
		}

		let ru_flags = elm('.tl-ru-flags').appendTo(node)
		for (let flag of NotaArea.flags) {
			let fel = elm('span.tl-ru-flag' + (ru.flags[flag] ? '.tl-ru-flag-on' : ''), flag, click => {
				if (fel.classList.toggle('tl-ru-flag-on')) {
					ru.flags[flag] = true
				} else {
					delete ru.flags[flag]
				}
			}).appendTo(ru_flags)
		}

		return node
	}

	trimText(s) {
		s = s + ''
		let a = []
		s.split('\n').map((s, i, ar) => {
			if (s != s.trimLeft()) {
				a.push(elm('span.bg_gray', s.slice(0, -s.trimLeft().length)))
			}
			a.push(s.trim())
			if (s.trim() && s != s.trimRight()) {
				a.push(elm('span.bg_gray', s.slice(s.trimRight().length)))
			}
			if (i != ar.length - 1) {
				a.push(elm('span.bg_gray', '↵'))
				a.push('\n')
			}
		})
		return a
	}

	timen_to_str(timen) {
		let s = (timen + '').replace(/(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/, (s, a, b, c, d, e) => `20${a}.${b}.${c} ${d}:${e}:00`)
		let d = new Date(s)
		return d.toLocaleString()
	}

	textarea_auto_resize_once(input) {
		input.style.height = 'auto'
		input.style.height = (input.scrollHeight + 10) + 'px'
	}

	textarea_auto_resize(...args) {
		function input() {
			this.style.height = 'auto'
			this.style.height = (this.scrollHeight + 10) + 'px'
		}
		return elm(...args, input)
	}

	textarea_calc_limit(str) {
		if (!this.en_limits) return
		let len = this.ui.pxlen(str)
		this.en_limits.q('.tl-en-limit').innerText = `${len}/${this.nota.limits.max}`
		this.en_limits.q('.tl-en-limit-filler').style.width = `${len/this.nota.limits.max*100}%`
		if (len <= this.nota.limits.ok) {
			this.en_limits.classList.remove('tl-limits-high')
			this.en_limits.classList.remove('tl-limits-overflow')
		} else if (len <= this.nota.limits.max) {
			this.en_limits.classList.add('tl-limits-high')
			this.en_limits.classList.remove('tl-limits-overflow')
		} else {
			this.en_limits.classList.remove('tl-limits-high')
			this.en_limits.classList.add('tl-limits-overflow')
		}

	}

	addTo(ui) {
		ui.table.prepend(this.elem)
		this.elem.classList.remove('tl-removed')
		this.removed = false
		return this
	}

	remove(off = false) {
		this.elem.classList.add('tl-removed')
		this.removed = true
		if (off)
			this.elem.remove()
		return this
	}

	async click_add() {
		let text = this.elem.q('.tl-en-edit').value
		if (text.match(/№\d/)) {
			if (this.nota.en_US.match(/]\d|\\[^c]\[\d/)) {
				if (!confirm('Проверь перевод как следует\nТочно нет ошибок с №?\nОни не будут исправлены'))
					return
			} else {
				this.elem.q('.tl-en-edit').value = text.replace(/№(\d)/g, '\\c[$1]')
				return
			}
		}
		if (!NotaArea.checkValidCharset(text)) {
			alert('Нет, ты не можешь отправлять всякую непечатную фигню!\n(перевод содержит неизвестный символ)')
			return
		}
		this.disable_node(this.elem.q('.tl-en'), false)
		if (this.nota.msg)
			this.applyValue(text)
		this.el_rus.classList.remove('tl-no-rus')

		if (window.ui && ui.focusedByKey) {
			nw.Window.get(ui.gameWindow).focus()
		}

		let ru = await this.nota.etc.translate(text, this.flags)
		let node = this.elmRu(ru)
		node.classList.add('tl-created')
		this.els_ru.shift(node)
		this.el_rus.prepend(node)
	}
	click_change(ru, node) {
		if (!NotaArea.isMod && NotaArea.user != ru.user)
			return alert('Только модераторы могут редактировать чужие переводы')

		if (!node.q('textarea')) {
			node.classList.add('tl-ru-editing')
			let t = this.textarea_auto_resize('textarea.tl-ru-text', //
				keyup => keyup.ctrlKey && keyup.code == 'Enter' && (keyup.preventDefault() + this.click_change(ru, node)) //
			)
			t.value = NotaArea.applyFlags(ru.raw_ru, false)
			node.q('.tl-ru-text').replaceWith(t)
			this.textarea_auto_resize_once(t)
			node.q('.tl-ru-change').innerText = '✔'
			node.q('.tl-ru-change').classList.add('tl-changed')
			return
		}

		let text = node.q('textarea').value
		if (text.match(/№\d/)) {
			if (ru.nota.en_US.match(/]\d|\\[^c]\[\d/)) {
				if (!confirm('Проверь перевод как следует\nТочно нет ошибок с №?\nОни не будут исправлены'))
					return
			} else {
				node.q('textarea').value = text.replace(/№(\d)/g, '\\c[$1]')
				return
			}
		}
		if (!NotaArea.checkValidCharset(text)) {
			alert('Нет, ты не можешь отправлять всякую непечатную фигню!\n(перевод содержит неизвестный символ)')
			return
		}
		if (!NotaArea.checkValidEscape(text)) {
			alert('Нет, ты не можешь отправлять всякую непечатную фигню!\n(перевод содержит неизвестную escape-последовательность)')
			return
		}

		NotaArea.makeNotaTlEdit(ru.href, ru.id, text, ru.flags)
		node.classList.add('tl-changed')
		this.disable_node(node)
		if (this.nota.msg)
			this.applyValue(text)

	}
	click_remove(ru, node) {
		if (!NotaArea.isMod && NotaArea.user != ru.user)
			return alert('Только модераторы могут удалять чужие переводы')

		if (!node.q('.tl-ru-remove').classList.contains('tl-removed'))
			return node.q('.tl-ru-remove').classList.add('tl-removed')

		NotaArea.makeNotaTlRemove(ru.href, ru.id)
		node.classList.add('tl-removed')
		this.disable_node(node)
	}
	click_voteup(ru, node) {
		if (NotaArea.user == ru.user)
			return alert('Нельзя изменить рейтинг своего перевода')

		let tnode = node.q('.tl-ru-text')
		let text = tnode.value || tnode.innerText
		ru.rating++
		node.q('.tl-ru-rate').innerText++
		NotaArea.makeNotaTlVote(ru.href, ru.id, 1)
		node.q('.tl-ru-plus').classList.add('tl-changed')
		this.disable_node(node)
		if (this.nota.msg)
			this.applyValue(text)
	}
	click_votedown(ru, node) {
		if (!NotaArea.isMod && NotaArea.user != ru.user)
			return alert('Только модераторы могут понижать рейтинг')
		if (NotaArea.user == ru.user)
			return alert('Нельзя изменить рейтинг своего перевода')

		NotaArea.makeNotaTlVote(ru.href, ru.id, -1)
		node.q('.tl-ru-minus').classList.add('tl-changed')
		this.disable_node(node)
	}

	disable_node(node, addEnable = true) {
		node.qq('button, textarea').concat(node).map(e => e.disabled = true)
		if (addEnable)
			this.add_enable_event(node)
	}
	add_enable_event(node) {
		node.oncontextmenu = node.ondblclick = event => {
			event.preventDefault()
			node.oncontextmenu = node.ondblclick = null
			this.enable_node(node)
		}
	}
	enable_node(node) {
		node.qq('button, textarea').concat(node).map(e => e.disabled = false)
	}

	applyValue(text) {
		if (!text.includes('№'))
			return this.nota.msg.value = this.nota.msg.value = this.nota.msg.data.de_DE = text

		let enm = this.nota.en_US.match(/\\[a-z](\[[^\]]+\])?/g) || []
		let rum = text.match(/№/g) || []
		if (enm.length != rum.length)
			console.warn('invalid translation: № dismatch', this)
		enm = [...enm]
		text = text.replace(/№/g, e => enm.shift())
		this.nota.msg.value = this.nota.msg.value = this.nota.msg.data.de_DE = text

		this.nota.ru = text
		this.nota.apply()
		this.nota.crossFile.saveAsync()
	}
}

GridUI = class GridUI {
	constructor() {
		this.nodeList = [];
		this.nodeMap = new Map()

		this.table = elm('table.tl-table.table')
		window.yatl && yatl.bind(this.table)

		this.onkeydown = keydown => {
			if (!keydown.ctrlKey || keydown.code != 'KeyL') return;
			keydown.preventDefault();
			this.clear(!keydown.shiftKey);
		}

		this.vars = {}

		this.addAlike = {
			over: {
				'item-name': {
					condition: n => n.file == 'data/item-database' && n.path.includes('.description'),
					alike: n => n.crossFile.infoMap.get(n.path.replace('.description', '.name'))
				},
			},
			under: {

			}
		}
	}

	updateLists(list1, list2) {}

	addAlikeOver(nota) {
		let matcher = this.addAlike.over
		for (let k in matcher) {
			if (matcher[k].condition(nota)) {
				this.addNota(matcher[k].alike(nota))
			}
		}
	}

	addAlikeUnder(nota) {
		for (let k in this.addAlike.under) {

		}
	}

	addNota(nota) {
		if (nota == null) {
			this.table.prepend(elm('tr.tl-node.tl-break', elm('td'), elm('td')))
			return this.table.prepend(elm('tr.tl-node', elm('td'), elm('td')))
		}
		if (nota instanceof NotaRu) {
			nota = nota.nota
		}
		if (nota && nota.data && nota.data.__path) {
			nota = this.getMsgNota(nota)
		}
		if (!(nota instanceof CrossEnRu)) {
			throw 'invalid object to add to grid'
		}
		if (this.lastNota == nota)
			return
		this.addAlikeUnder(nota)

		if (!nota.node) {
			let node = new GridNode(nota, this)
			this.nodeList.push(node)
			this.nodeMap.set(node.nota.uniq, node)
		}

		this.lastNota = nota
		nota.node.addTo(this)

		this.addAlikeOver(nota)

		return nota.node
	}
	textarea_auto_resize_once(textarea) {
		textarea.classList.add('auto_resize')
		textarea.rows = 1
		let line = parseFloat(getComputedStyle(textarea).height)
		textarea.rows = Math.round(textarea.scrollHeight / line)
		textarea.classList.remove('auto_resize')
	}

	makeUI(clear = false) {
		if (clear) {
			q('body').innerHTML = ''
			this.lastNota = -1
		}

		let container = elm('#find-container').appendTo('body')


		elm('textarea#find-file[rows=1][placeholder=путь][title=должно содержаться в имени файла, пути или описании]', //
			keydown => this.keydown(keydown), keyup => this.keyup(keyup), input => this.textarea_auto_resize_once(input.target)).appendTo(container)
		elm('textarea#find-en[rows=1][placeholder=англ][title=должно содержаться в оригинальном тексте]', //
			keydown => this.keydown(keydown), keyup => this.keyup(keyup), input => this.textarea_auto_resize_once(input.target)).appendTo(container)
		elm('textarea#find-ru[rows=1][placeholder=рус][title=должно содержаться в тексте перевода. "+"-есть переводы, "-"-нет переводов]', //
			keydown => this.keydown(keydown), keyup => this.keyup(keyup), input => this.textarea_auto_resize_once(input.target)).appendTo(container)

		let count = elm('#find-count').appendTo(container)
		elm('small[style=color:gray]', ' x ').appendTo(count)
		elm('span#find-count-value', 0).appendTo(count)

		let btns = elm('#find-btns').appendTo(container)
		elm('button#find-btn-add[title=Выведет найденные результаты. Можно также нажать Enter]', '⌕', click => ui.addFound()).appendTo(btns)
		elm('button#find-btn-clear[title=Очистить последний поиск. Можно также нажать Ctrl+L или Ctrl+Shift+L]', '⎚', click => ui.clear(true)).appendTo(btns)
		elm('button#find-btn-restart[title=Перезапустить игру]', '↺', click => buttonActions.restartApp(this.gameWindow ? '' : 'onlyGrid')).appendTo(btns)

		this.table.appendTo('body')
		return this
	}

	async init(w = null) {
		this.gameWindow = w
		if (w) {
			this.gameWindow = w
			while (!w.ig)
				await Promise.wait(100)
			this.langedit = Object.values(w.ig).filter(e => e && e.name == 'LangEdit')[0]
			while (!this.langedit) {
				await Promise.wait(100)
				this.langedit = Object.values(w.ig).filter(e => e && e.name == 'LangEdit')[0]
			}
			this.langstrs = Object.values(this.langedit).filter(e => Array.isArray(e))
			while (!this.langstrs || !this.langstrs.length) {
				await Promise.wait(100)
				this.langstrs = Object.values(this.langedit).filter(e => Array.isArray(e))
			}
			this.oldstrs = this.langstrs.map(e => [])

			log('ui inited')

			if (!w.sc.voiceActing.active && localStorage.getItem('bonus-Best-VA') == 'off'
				|| w.sc.voiceActing.active && localStorage.getItem('bonus-Best-VA') == 'on')
						w.sc.voiceActing.toggle()
			if (!w.sc.gameCode.isEnabled('Caramelldansen') && localStorage.getItem('bonus-Caramelldansen') == 'on'
				|| w.sc.gameCode.isEnabled('Caramelldansen') && localStorage.getItem('bonus-Caramelldansen') == 'off')
					w.sc.gameCode.enterCode('Caramelldansen', true) // или тут и false?
			if (!w.sc.gameCode.isEnabled('WoN-Boots') && localStorage.getItem('bonus-WoN-Boots') != 'off')
					w.sc.gameCode.enterCode('WoN-Boots', true) // тут только off
			if (!w.sc.gameCode.isEnabled('Holiday-Man') && localStorage.getItem('bonus-Holiday-Man') != 'off')
					w.sc.gameCode.enterCode('Holiday-Man', true) // тут только off

			w.document.body.addEventListener('keydown', this.onkeydown)
			this.initializeLimiter()
			this.initializeFocuser()

			w.ontext = e => {
				console.log(e);
				ui.addNota(e)
			}
		}
		document.body.addEventListener('keydown', this.onkeydown)
		this.interval = setInterval(() => this.update(), 100)
		this.find()
		return this
	}

	pxlen(s) {
		if (!this.gameWindow)
			return -1

		if (!this.pxWmap) {
			this.pxWmap = Object.fromEntries(this.gameWindow.sc.fontsystem.font.widthMap.map((e, i) => [String.fromCharCode(32 + i), e]).filter(e => e[1] > 1))
			this.initializeLimiter(true)
			this.gameWindow.pxlen = s => this.pxlen(s)
		}

		return NotaArea.applyFlags(s, false).replace(/\\[.!cvis](\[.*?\])?|№/g, '').split('').map(e => this.pxWmap[e]).reduce((v, e) => v + e + 1, -1)
	}

	initializeLimiter(err = false) {
		const recommendedItemLen = 230 / 2
		const maxItemLen = 240 / 2

		const recommendedBottomLen = 477
		const maxBottomLen = 550

		this.pxlen('')

		let names = nn.filter(e => e.file == 'data/item-database' && e.path && e.path.match(/^items\.\d+\.name$/))
		names.map(e => e.limits = { max: maxItemLen, ok: recommendedItemLen })
		let descriptions = nn.filter(e => e.file == 'data/item-database' && e.path && e.path.match(/^items\.\d+\.description/))
		descriptions.map(e => e.limits = { max: maxBottomLen, ok: recommendedBottomLen })

		if (err) {
			console.enterGroup('invalid translation: text is way too long')
			for (let n of [names, descriptions].flat()) {
				for (let r of n.etc.rus) {
					if (this.pxlen(r.raw_ru) > n.limits.max) {
						n.etc.err_width_overflow = true
						if (!r.err.includes('err_width_overflow'))
							r.err += ' err_width_overflow '
						console.warnGroup('invalid translation: text is way too long', n.etc)
					}
				}
			}
			console.exitGroup('invalid translation: text is way too long')
		}
	}

	initializeFocuser() {
		this.focuserFn = (event) => {
			if (event.code != 'Backquote') return
			event.preventDefault()
			event.stopPropagation()
			q('textarea.tl-en-edit').focus()
			nw.w.focus()
			this.focusedByKey = true
		}
		this.gameWindow.addEventListener('keydown', this.focuserFn)
	}

	getMsgNota(msg) {
		if (msg.nota)
			return msg.nota
		console.log(msg)
		let [fname, path] = msg.data.__path.split(' ')
		let uniq = `${fname} / ${path} / ${msg.data.langUid} / ${msg.data.en_US}`
		msg.uniq = uniq
		for (let n of NotaArea.areas) {
			if (n.notaMap.has(uniq)) {
				let nota = n.notaMap.get(uniq)
				nota.msg = msg
				return msg.nota = nota
			}
		}
		let cf = CrossFile.at(fname)
		if (cf) {
			cf.load().generateEnRus()
			let c = cf.infoMap.get(path)
			if (c) {
				c.msg = msg
				return msg.nota = c
			}
		}

		return null
	}

	get maxChildren() {
		return 500
	}

	deleteSelf() {
		clearInterval(this.interval)
		document.body.removeEventListener('keydown', this.onkeydown)
		if (this.gameWindow && !this.gameWindow.closed) {
			this.gameWindow.document.body.removeEventListener('keydown', this.onkeydown)
		}
		this.deleted = true
	}

	update() {
		if (this.table.closest('body') != document.body)
			return this.deleteSelf()

		if (!this.gameWindow)
			return

		// for (let i = 0; i < this.langstrs[0].length; i++) {
		//     if (this.oldstrs[0][i] != this.langstrs[0][i])
		//         this.getMsgNota(this.langstrs[0][i]) && this.addNota(this.langstrs[0][i].nota)
		// }
		if (this.langstrs[1].length == 0 && this.oldstrs[1].length != 0)
			this.addNota(null)

		for (let i = 0; i < this.langstrs[1].length; i++) {
			let msg = this.langstrs[1][i]
			if (this.oldstrs[1][i] && this.oldstrs[1][i].content.data.__path == msg.content.data.__path)
				continue;
			this.getMsgNota(msg.content) && this.addNota(msg.content.nota)
			window.scroll({ top: 0, behavior: 'smooth' })
		}

		let set = new Set(this.langstrs.flat())
		this.oldstrs.filter(e => !set.has(e)).filter(e => e.nota).map(e => e.nota.node.remove())

		this.oldstrs = this.langstrs.map(e => e.slice())

		while (this.table.childElementCount > this.maxChildren)
			this.table.lastElementChild.remove()
	}

	clear(untilBreak = false) {
		this.lastNota = -1
		if (untilBreak) {
			while (ui.table.firstElementChild && !ui.table.firstElementChild.matches('.tl-break'))
				ui.table.firstElementChild.remove()
			while (ui.table.firstElementChild && ui.table.firstElementChild.matches('.tl-break'))
				ui.table.firstElementChild.remove()
		} else {
			this.table.qq('tr').map(e => e.remove())
		}
	}

	keydown(event) {
		if (event && event.code && event.code.endsWith('Enter') && !event.ctrlKey && !event.shiftKey) {
			event.preventDefault()
			return this.addFound()
		}

		if (event && event.code && event.code.endsWith('Delete') && event.target && !event.target.value)
			qq('#find-file, #find-en, #find-ru').map(e => (e.value = '', this.textarea_auto_resize_once(e)))
	}

	async keyup(event) {
		let p = this.find_wait = Promise.empty()
		Promise.wait(250).then(p.r)
		await p.p
		if (p != this.find_wait) return
		this.find()
	}

	make_filter(find_file, find_en, find_ru) {
		find_file = typeof find_file == 'string' ? find_file : q('#find-file').value //.split(' ').filter(e => e.length);
		find_en = typeof find_en == 'string' ? find_en : q('#find-en').value //.toLowerCase().split(' ').filter(e => e.length);
		find_ru = typeof find_ru == 'string' ? find_ru : q('#find-ru').value //.toLowerCase().split(' ').filter(e => e.length);
		let find_gen = []
		let fn_sort = null;

		let lastNota = () => this.lastNota // this.langstrs && this.langstrs[1][0].nota
		let curLoc = () => {
			if (!this.gameWindow) return ''
			return lastNota() ? lastNota().file : '//err_nowhere'
		}
		let curEntity = () => {
			if (!this.gameWindow) return ''
			if (!lastNota()) return '//err_nowhere'
			let a = lastNota().path.split('.')
			return a.slice(0, a.indexOf('entities') + 2).join('.')
		}
		let atQuest = () => {
			if (!this.gameWindow) return ''
			if (!lastNota()) return '//err_nowhere'
			let a = lastNota().path.split('.')
			let b = CrossFile.at(lastNota().file).getNode(a.slice(0, a.indexOf('quest') + 1).join('.'))
			if (!Array.isArray(b)) return '//err_nowhere'
			let c = b.find(e => e.quest).quest
			return `"data/database quests.${c}"`

		}
		let negOrSame = (neg, fn) => neg ? c => !fn(c) : fn
		let timeToReal = (time) => ((time || 0) + '').match(/\d?\d(?=(\D*\d\d)*\D*$)/g).map((e, i, a) => [+e, a.length - 1 - i]).reduce((v, [e, i]) => v * [60, 60, 24, 31, 12, 100][i] + e, 0)

		find_file = find_file //
			.replace('@this', s => `@here ${curEntity()}`)
			.replace('@mapname', s => `data/areas/${curLoc() && curLoc().split('/')[2]} floors. .maps. .name`)
			.replace('@here', s => curLoc())
			.replace('@hax', '"IF newgame.sergey-hax"')
			.replace('@quest', s => atQuest())
			.replace(/(-?)@(err_\w+)/g, (s, neg, a) => {
				find_gen.push(negOrSame(neg, c => c.etc[a]))
				return ''
			})
			.replace('@err', s => (find_gen.push(c => c.etc.rus.find(e => e.err)), ''))
			.replace(/(-?)@flag(:([^:⟫\s]*))?(:([^:⟫\s]*))?/g, (s, neg, kk, k, vv, v) => {
				find_gen.push(
					vv ? negOrSame(neg, c => c.etc.rus.find(r => r.flags[k] == v)) :
					kk ? negOrSame(neg, c => c.etc.rus.find(r => r.flags[k])) :
					negOrSame(neg, c => c.etc.rus.find(r => Object.values(r.flags).length))
				)
				return ''
			})
			.replace(/(-?)@by:([^:⟫\s]*)/g, (s, neg, user) => {
				find_gen.push(negOrSame(neg, c => c.etc.rus.find(r => r.user == user)))
				return ''
			})
			.replace(/(-?)@time:([^:⟫\s]*)(:([^:⟫\s]*))?/g, (s, neg, time, dd, delta) => {
				time = timeToReal(time), delta = timeToReal(delta)
				find_gen.push(
					dd ? negOrSame(neg, c => c.etc.rus.find(r => Math.abs(r._find_time - time) < delta)) :
					negOrSame(neg, c => c.etc.rus.find(r => r._find_time == time)))
				return ''
			})
			.replace(/@latest/g, s => {
				find_gen.push(c => c._find_time)
				fn_sort = (a, b) => b.etc.rus[0].time - a.etc.rus[0].time
				return ''
			})
			.replace(/(-?)@simple/g, (s, neg) => {
				find_gen.push(negOrSame(neg, c => c.simple))
				return ''
			})
			.replace(/(-?)@diff/g, (s, neg) => {
				find_gen.push(negOrSame(neg, c => c.ru != c.enruNode().de_DE))
				return ''
			})
			.replace(/(-?)@tsv:checked/g, (s, neg) => {
				find_gen.push(negOrSame(neg, c => c.tableNode && c.tableNode.checked))
				return ''
			})
			.replace(/(-?)@tsv/g, (s, neg) => {
				find_gen.push(negOrSame(neg, c => c.tableNode && c.tableNode.ru_RU == c.enruNode().de_DE))
				return ''
			})
			.replace(/(-?)@translated/g, (s, neg) => {
				find_gen.push(negOrSame(neg, c => c.enruNode().en_US != c.enruNode().de_DE))
				return ''
			})


		log(find_file)


		let fn_includes = v => s => s.includes(v)
		let fn_excludes = v => s => !s.includes(v)

		let fn_has_ru = c => !!c.etc.rus.length
		let fn_hasnt_ru = c => !c.etc.rus.length
		let fn_any_ru = c => true

		function filters(find, retBool = false) {
			let has = [],
				hasnt = [],
				bool = []
			//           (    1   ) (  2  )  (  3 )( 4 ) ( 5 )
			let regex = /(-|\+|^| )"([^"]+)"|(-|\+)(\S*)|(\S+)/g
			find = find.toLowerCase()
			let m;
			while ((m = regex.exec(find)) != null) {
				if (m[2]) {
					if (m[1] == '-') hasnt.push(m[2])
					if (!m[1] || m[1] == '+') has.push(m[2])
					continue
				}
				if (m[3]) {
					if (!m[4]) {
						if (retBool) {
							if (m[3] == '-') return fn_hasnt_ru
							if (m[3] == '+') return fn_has_ru
						}
						continue
					}
					if (m[3] == '-') hasnt.push(m[4])
					if (m[3] == '+') has.push(m[4])
					continue
				}
				if (m[5]) has.push(m[5])
			}
			// log(has, hasnt, bool)
			if (retBool) {
				return fn_any_ru
			}
			let fns = [has.map(e => fn_includes(e)), hasnt.map(e => fn_excludes(e))].flat()
			return fns
		}

		find_file = filters(find_file)
		find_en = filters(find_en)
		find_gen.push(filters(find_ru, true) || fn_any_ru)
		find_ru = filters(find_ru)

		let prep = c => {
			if (c._prepared) return
			c._find_fstr = `${c.file} ${c.path} ${c.description}`.toLowerCase()
			c._find_enstr = '$' + c.enru.en_US.toLowerCase() + '$'
			c._find_rustrs = c.etc.rus.map(e => '$' + e.ru.toLowerCase().trim() + '$')
			if (!c._find_rustrs.length && c.tableNode && c.tableNode.ru_N)
				c._find_rustrs.push(c.tableNode.ru_apply.toLowerCase())
			c.etc.rus.map(e => e._find_time = timeToReal(e.time))
			c._find_time = c.etc.rus[0] && c.etc.rus[0]._find_time || 0
			c._prepared = true
		}

		function filter(c) {
			if (!c._prepared) {
				prep(c)
			}

			for (let fn of find_gen) {
				if (!fn(c))
					return false
			}

			if (find_file.length) {
				let fstr = c._find_fstr
				for (let fn of find_file)
					if (!fn(fstr))
						return false
			}

			if (find_en.length) {
				let enstr = c._find_enstr
				for (let fn of find_en)
					if (!fn(enstr))
						return false
			}

			if (find_ru.length) {
				let rustrs = c._find_rustrs
				if (!rustrs.find(rustr => {
						for (let fn of find_ru) {
							if (!fn(rustr))
								return false
						}
						return true
					})) return false
			}

			return true
		}
		filter.sorter = fn_sort
		return filter
	}

	find(...a) {
		let all = this.all || (this.all = nn.filter(e => e.simple != true))

		// console.time('filter')
		let fn = typeof a[0] == 'function' ? a[0] : this.make_filter(...a)
		let items = this.all.filter(fn)
		if (fn.sorter) items.sort(fn.sorter)
		// console.timeEnd('filter')

		q('#find-count-value').innerText = items.length

		return this.found = items
	}

	addFound(...a) {
		let items = this.find(...a)
		items = items.slice(0, this.maxChildren).reverse()
		log(items)
		while (this.table.childElementCount + items.length > this.maxChildren)
			this.table.lastElementChild.remove()
		this.list(items)
		window.scroll({ top: 0, behavior: 'smooth' })
	}

	list(list) {
		if (!(list instanceof Array))
			list = Array.from(list)
		if (!list.length) return

		if (list[0] instanceof NotaRu) {
			list = list.map(e => e.nota)
		}
		if (!(list[0] instanceof CrossEnRu))
			throw list

		this.addNota(null)
		list.map(e => this.addNota(e))
		return list
	}

}
