CrossEnRu = class CrossEnRu {
	constructor() {
		this.description = ''
		this.langUid = ''
		this.en_US = ''
		this.ru = ''
		this.fileIndex = 0
		this.index = 0
	}

	setEn(en, force = false) {
		if (this.en_US && !force)
			return
		let [data, text = ''] = en.trim().split('\n\n')
		let [info, ...desc] = data.split('\n').map(e => e.trim())
		let [file, path, langUid] = data.split(/\s+/)

		this.file = file
		this.path = path
		if (langUid && langUid.match(/^#\d+$/))
			this.langUid = +langUid.slice(1)

		this.en_US = text.trimRight()
		this.description = desc.join('\n')
		return this
	}
	setFile(f) {
		this.crossFile = f
		return this
	}
	setFileIndex(i) {
		this.fileIndex = i
		return this
	}
	setIndex(i) {
		this.index = i
		return this
	}
	setEtc(data) {
		this.etc = data
		return this
	}
	setRaw(raw) {
		this.raw = raw
		this.file = raw.split('\n\n')[1]
		return this
	}
	get ord() {
		return 10000 * this.fileIndex + 2 * this.index
	}
	get rawEn() {
		if (this.raw)
			return this.raw
		return `${this.file}.json ${this.path} ${this.langUid && '#'}${this.langUid}${this.description && '\n'}${this.description}\n\n${this.en_US}`
	}
	get uniq() {
		if (this.raw)
			return `raw: ${this.raw}`
		return `${this.file} / ${this.path} / ${this.langUid} / ${this.en_US}`
	}

	get simple() {
		let s1 = this.en_US.replace(/\\[a-z](\[[^\]]+\])?/g, '')
		if (s1.replace(/[\W\d]/g, '') == '' || !this.index) {
			Object.defineValue(this, 'simple', true)
			return true
		}

		let s2 = s1.replace(/\b(Lea|Hi|Bye|nods|Thanks|Up|Down|Yes|No|Ok|Cancel|Logout|Wait|Why|shakes head|How)\b/gi, '').replace(/[\W\d]/g, '')
		if (s2 == '') {
			Object.defineValue(this, 'simple', 'const')
			return 'const'
		}

		Object.defineValue(this, 'simple', false)
		return false
	}

	get best() {
		return this.etc.rus[0]
	}


	setTr(etc) {
		this.etc = etc
		this.href = etc.href.split('#')[0]
		if (etc.en.startsWith('----'))
			this.setRaw(etc.en)
		else {
			if (!this.en)
				this.setEn(etc.en)
			this.ru = etc.ru;
		}

		return this
	}

	cloneFrom(enru) {
		this.setEn(enru.rawEn, 1)
		this.setIndex(enru.index)
		this.setFileIndex(enru.fileIndex)
		this.setFile(enru.crossFile)
		return this
	}

	toString() {
		return this.rawEn
	}

	apply(time) {
		if (this.ru && this.crossFile && this.en_US != this.ru) {
			let node = this.crossFile.getNode(this.path)
			if (node.de_DE == this.ru)
				return
			if (node.de_DE != node.en_US)
				node.de_PREV = node.de_DE
			node.de_DE = this.ru
			this.crossFile.__modified = time

		}
	}

	enruNode() {
		return this.crossFile.getNode(this.path)
	}
	get enru() {
		return this.crossFile.getNode(this.path)
	}

	notaUpload() {
		if (this.href)
			return NotaArea.makeNotaEdit(this.href, this.ord, this.rawEn)
		else
			return NotaArea.makeNotaEdit(NotaArea.areaMap[this.crossFile.__area].href + '/0', this.ord, this.rawEn);
	}
	notaRemove() {
		return NotaArea.makeNotaRemove(this.href)
	}
}