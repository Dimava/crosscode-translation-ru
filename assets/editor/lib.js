Promise.empty = (r, j, p) => (p = new Promise((res, rej) => (r = res,
	j = rej)), {
	r,
	j,
	p
});
fetch.doc = async function(url) {
	window.console.log('fetching document:', url);
	let xhr = new XMLHttpRequest();
	let p = Promise.empty();
	xhr.onload = p.r;
	xhr.open("GET", url);
	xhr.responseType = "document";
	xhr.send();
	await p.p;
	let document = xhr.responseXML;
	document.redirected = xhr.redirected;
	document.base = xhr.redirected ? null : url;
	document.url = function(a) {
		if (typeof a == 'function')
			a = a();
		if (typeof a == 'object') {
			if (a.href)
				a = a.getAttribute('href');
			else if (a.src)
				a = a.getAttribute('src');
		}
		if (typeof a == 'string') {
			return new URL(a, document.base).href;
		}
		throw todo();
	}
	return document;
}

void

function initQ(window) {
	function q(s, el = this) {
		return el.querySelector(s);
	}

	function qq(s, el = this) {
		return [...el.querySelectorAll(s)];
	}

	window.HTMLElement.prototype.q = q;
	window.HTMLElement.prototype.qq = qq;
	window.HTMLElement.prototype.appendTo = function(e) {
		if (typeof e == 'string')
			e = window.document.q(e);
		e.append(this);
		return this;
	}

	window.Window.prototype.q = function winq(...a) {
		return (this || window).document.documentElement.q(...a);
	}
	window.Window.prototype.qq = function winqq(...a) {
		return (this || window).document.documentElement.qq(...a);
	}

	window.Document.prototype.q = function winq(...a) {
		return this.documentElement.q(...a);
	}
	window.Document.prototype.qq = function winqq(...a) {
		return this.documentElement.qq(...a);
	}

}(window);

Object.fromEntries = Object.fromEntries || function fromEntries(iterable) {
	return [...iterable].reduce((obj, [key, val]) => {
		obj[key] = val
		return obj
	}, {})
}
Object.defineGetter = function(o, name, get = name) {
	if (typeof name == 'function')
		name = name.name;
	return Object.defineProperty(o, name, {
		configurable: true,
		get
	});
}
Object.defineValue = function(o, name, value = name, enumerable = false) {
	if (typeof name == 'function')
		name = name.name;
	return Object.defineProperty(o, name, {
		enumerable,
		configurable: true,
		writable: true,
		value
	});
}
Object.keys = o => Object.entries(o).map(e => e[0])

fs = require('fs')

fs.ensureSync = function ensureDirectoryExistence(file) {
	let dir = file.split('/').slice(0, -1).join('/')
	if (fs.existsSync(dir))
		return
	fs.ensureSync(dir)
	fs.mkdirSync(dir)
}

fs.copySync = function copySync(file, to) {
	let stat = fs.lstatSync(file)
	if (stat.isDirectory()) {
		if (!file.endsWith('/'))
			file += '/'
		if (!to.endsWith('/'))
			to += '/'
		if (!fs.existsSync(to))
			fs.mkdirSync(to)
		for (let fname of fs.readdirSync(file)) {
			copySync(file + fname, to + fname)
		}
	}
	if (stat.isFile()) {
		fs.copyFileSync(file, to)
	}
}

setProgress = function(i = -1, a = []) {
	let p = q('progress') || elm('progress').appendTo('body')
	p.max = a.length
	p.value = i + 1
	let s = q('span#progressText') || elm('span#progressText').appendTo('body')
	s.innerText = ` ${i + 1} / ${a.length}`
	return true
}

Array.prototype.pcmap = function(fn) {
	return this.map((e, i, a) => setProgress(i, a) && fn(e, i, a))
}
Array.prototype.ppmap = function(fn, ...args) {
	let done = 0
	return this.pmap(async (e, i, a) => {
		setProgress(done - 1, a)
		let v = await fn(e, i, a)
		setProgress(done++, a)
		return v
	}, ...args)
}

Array.prototype.pwmap = async function(fn) {
	// fn is *syncronous* function
	const minFrameLength = 20
	let result = Array(this.length)
	let frame = Promise.frame()
	let now = performance.now()
	setProgress(0, this)
	for (let i = 0; i < this.length; i++) {
		if (performance.now() - now > minFrameLength) {
			now = await frame;
			frame = Promise.frame()
		}
		result[i] = fn(this[i], i, this)
		setProgress(i, this)
	}
	await frame
	return result
}
Array.prototype.pmap = async function(fn, threads = 5) {
	let result = Array(this.length)
	if (!((threads = +threads) > 0))
		threads = 1e6
	let activeThreads = 0

	let any = Promise.empty()
	for (let i = 0; i < this.length; i++) {
		activeThreads++
		fn(this[i], i, this).then(v => result[i] = v).catch(e => result[i] = e).then(() => any.r(activeThreads--))
		if (activeThreads >= threads) {
			await any.p
			any = Promise.empty()
		}
	}
	while (activeThreads) {
		await any.p
		any = Promise.empty()
	}
	return result
}
Array.prototype.unique = function() {
	return Array.from(new Set(this))
}

Promise.wait = Promise.delay = (t = 10) => new Promise(r => setTimeout(r, t));
Promise.wait = async function(t, n = 1) {
	let val = await this;
	await new Promise(r => setTimeout(r, t));
	if (typeof n == 'number') {
		for (n--; n > 0; n--)
			await new Promise(r => setTimeout(r, t));
	}
	return val;
}
Promise.frame = (n) => +n ? Promise.resolve().frame(n) : new Promise(r => requestAnimationFrame(r));
Promise.empty = (r, j) => ({
	p: new Promise((res, rej) => (r = res,
		j = rej)),
	r,
	j
});

Promise.prototype.wait = Promise.prototype.delay = async function(t = 10, n = 1) {
	let val = await this;
	await Promise.wait(t, n);
	return val;
};
Promise.prototype.frame = async function(n) {
	let val = await this;
	await new Promise(r => requestAnimationFrame(r));
	if (typeof n == 'number') {
		for (n--; n > 0; n--)
			await new Promise(r => requestAnimationFrame(r));
	}
	return val;
}

elm = function elm(sel = '', ...children) {
	let tag = 'div',
		cls = [],
		id = '',
		attrs = [];;
	sel = sel.replace(/^[\w\-]+/, (s => (tag = s,
		'')));

	sel = sel.replace(/\[(.*?)=(".*?"|'.*?'|.*?)\]/g, (s, attr, val) => (attrs.push({
			attr,
			val
		}),
		''));

	sel = sel.replace(/\.([\w\-]+)/g, (s, cl) => (cls.push(cl),
		''));

	sel = sel.replace(/\#([\w\-]+)/g, (s, d) => (id = id || d,
		''));

	if (sel != '')
		alert('sel is not empty!\n' + sel);

	let e = document.createElement(tag);

	if (id)
		e.id = id;

	if (cls.length)
		e.className = cls.join(' ');

	attrs.forEach(({ attr, val }) => {
		e.setAttribute(attr, val);
	});

	if (children.length) {
		e.append(...children.filter(e => typeof e != 'function'));
		for (let fn of children.filter(e => typeof e == 'function')) {
			let fname = fn.name || (fn + '').match(/\w+/)[0]
			fname = fname.startsWith('on') ? fname : 'on' + fname
			e[fname] = fn
		}
	}

	return e;
}

log = console.log.bind(console)

console._groupList = []
console._activeGroupList = []

console.warnGroup = function(...args) {
	console.enterGroup(true)
	console.warn(...args)
}

console.enterGroup = function(group) {
	if (group === true) {
		for (let i = console._activeGroupList.length; i < console._groupList.length; i++) {
			console.groupCollapsed(console._groupList[i])
			console._activeGroupList.push(console._groupList[i])
		}
		return
	}
	if (console._groupList.includes(group)) {
		return
	}
	console._groupList.push(group)
}

console.exitGroup = function(group) {
	if (!group) {
		while (console._activeGroupList.length) {
			console.groupEnd()
			console._activeGroupList.length.pop()
		}
		console._groupList = []
		return
	}
	if (!console._groupList.includes(group)) {
		return
	}
	while (console._groupList.pop() != group) {}
	while (console._activeGroupList.length > console._groupList.length) {
		console.groupEnd()
		console._activeGroupList.pop()
	}
}


fs.rmdirAll = function(folder) {
	if (!fs.existsSync(folder))
		return
	for (let file of fs.readdirSync(folder)) {
		let path = folder + '/' + path
		if (fs.lstatSync(path).isDirectory())
			fs.rmdirAll(path)
		else
			fs.unlinkSync(path)
	}
	fs.rmdirSync(folder)
}

nw.w = nw.w || nw.Window.get(this)

Error.prototype.toString = function() {
	return this.stack
}

if (localStorage.getItem('Dimava-mode')) {
	function beep(vol = 20, freq = 100, duration = 3000) {
		let a = beep.a = beep.a || new AudioContext()
		let v = a.createOscillator()
		let u = a.createGain()
		v.connect(u)
		v.frequency.value = freq
		v.type = "square"
		u.connect(a.destination)
		u.gain.value = vol * 0.01
		v.start(a.currentTime)
		v.stop(a.currentTime + duration * 0.001)
	}

	void

	function() {
		let t = 0;
		setInterval(() => {
			if (performance.now() - t > 5000) {
				beep(20, 100, 300)
			}
			t = performance.now()
		}, 100)
	}()
}
