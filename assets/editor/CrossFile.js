loc0 = './assets'
add_paths_to_enru = true

CrossFile = class CrossFile {
	static at(fpath) {
		if (!fpath)
			return null
		if (!this.map)
			this.map = new Map()
		if (this.map.has(fpath))
			return this.map.get(fpath)
		fpath = fpath.split('/assets/').pop().split('.json')[0]
		if (this.map.has(fpath))
			return this.map.get(fpath)
		let f = new CrossFile(fpath)
		if (!fs.existsSync(f.__full_path))
			return null
		this.map.set(fpath, f)
		return f
	}
	constructor(fpath) {
		this.__path = fpath.split('/assets/').pop().split('.json')[0]
		if (!fs.existsSync(this.__full_path))
			return null
		if (!CrossFile.map)
			CrossFile.map = new Map()
		if (!CrossFile.map.has(this.__path))
			CrossFile.map.set(this.__path, this)
	}

	get __full_path() {
		return `${loc0}/${this.__path}.json`
	}
	get __back_path() {
		return `${loc0}/${this.__path}.json.backup`
	}
	async loadAsync(force = false) {
		if (this.__loaded && !force)
			return this
		let p = Promise.empty()
		fs.readFile(this.__full_path, (e, r) => p.r(r))
		Object.assign(this, JSON.parse(await p.p))
		this.__loaded = true
		this.__modified = false
		return this
	}
	load(force = false) {
		if (this.__loaded && !force)
			return this
		Object.assign(this, JSON.parse(fs.readFileSync(this.__full_path)))
		this.__loaded = true
		this.__modified = false
		return this
	}
	saveBackup() {
		if (fs.existsSync(this.__back_path))
			return this
		fs.writeFileSync(this.__back_path, fs.readFileSync(this.__full_path))
		return this
	}
	loadBackup() {
		this.saveBackup()
		for (let key in this)
			if (key != '__path')
				delete this[key]
		Object.assign(this, JSON.parse(fs.readFileSync(this.__back_path)))
		this.__loaded = true
		this.__modified = -1
		this.applyCustomMigration()
		return this
	}
	applyCustomMigration() {
		if (this.__path == 'data/database') {
			this.__modified = 2
			delete this.enemies['jungle.special.parrot-gangster- boss-1'];
			for (let k in this.areas) {
				this.areas[k].shortName = this.areas[k].shortName || JSON.parse(JSON.stringify(this.areas[k].name))
			}
		}
	}

	static findFiles(force = false, path = loc0) {
		if (path.endsWith('/'))
			path = path.slice(0, -1)

		if (fs.existsSync(`${path}/jsonFiles`) && !force) {
			return JSON.parse(fs.readFileSync(`${path}/jsonFiles`))
		}

		console.time('CrossFile.findFiles')
		let allFileLocs = []

		function dig_file_paths(path) {
			if (fs.statSync(path).isFile()) {
				allFileLocs.push(path)
				return
			}
			fs.readdirSync(path).map(e => dig_file_paths(`${path}/${e}`))
		}
		dig_file_paths(path)
		let jsonLocs = allFileLocs.filter(e => e.endsWith('.json'))//.filter(e => !e.includes('/credits/'))
		fs.writeFileSync(`${path}/jsonFiles`, JSON.stringify(jsonLocs))
		console.timeEnd('CrossFile.findFiles')
		return jsonLocs
	}

	static async findTextFilesAsync(force = false, path = loc0) {
		if (path.endsWith('/'))
			path = path.slice(0, -1)

		if (fs.existsSync(`${path}/filesWithText`) && !force) {
			return JSON.parse(fs.readFileSync(`${path}/filesWithText`))
		}

		let allFileLocs = this.findFiles(true, path)
		let textFileLocs = []

		console.time('CrossFile.findTextFiles')
		await allFileLocs.pwmap(fpath => { if (CrossFile.at(fpath).loadBackup().findTexts().__has_text) { textFileLocs.push(fpath) } })
		fs.writeFileSync(`${path}/filesWithText`, JSON.stringify(textFileLocs))
		console.timeEnd('CrossFile.findTextFiles')
		return textFileLocs
	}

	static findTextFiles(force = false, path = loc0) {
		if (this.foundTextFiles && !force) {
			return this.foundTextFiles
		}
		if (fs.existsSync(`${path}/filesWithText`) && !force) {
			return this.foundTextFiles = JSON.parse(fs.readFileSync(`${path}/filesWithText`))
		}
		return null
	}

	static getFiles() {
		if (this.files)
			return this.files
		return this.files = this.findTextFiles().map(e => CrossFile.at(e).load())
	}

	unload() {
		this.__unloaded = true
		let keys = Object.keys(this).filter(e=>!e.startsWith('_'))
		for (let path of Object.keys(this.__text_paths)) {
			this[path] = this.getNode(path)
		}
		for (let k of keys) {
			delete this[k]
		}
	}

	async saveAsync() {
		this.saveBackup()
		await new Promise(r => fs.writeFile(this.__full_path, JSON.stringify(this), r))
		return this
	}

	save() {
		if (this.__unloaded) return
		this.saveBackup()
		this.__modified = false
		fs.writeFileSync(this.__full_path, JSON.stringify(this))
		return this
	}

	static async saveAllAsync(onlyChanged = true) {
		let filesToSave = this.getFiles().map(e => e.load()).filter(e => !onlyChanged || e.__modified)
		console.log('filesToSave:', filesToSave)
		await filesToSave.ppmap(e => e.saveAsync(), 50)
		this.at('data/LANG').save()
		this.splitLang().map(e => e.save())
	}

	async saveAtAsync(loc) {
		let path = `${loc}/${this.__path}.json`
		fs.ensureSync(path)
		await new Promise(r => fs.writeFile(path, JSON.stringify(this), r))
		return this
	}

	saveAt(loc) {
		let path = `${loc}/${this.__path}.json`
		fs.ensureSync(path)
		fs.writeFileSync(path, JSON.stringify(this))
		return this
	}

	findTexts(force = false) {
		if (this.__text_paths && !force)
			return this

		function dig_find_text(file, path = [], result = {}) {
			if (typeof file != 'object' || !file)
				return false
			if (file.en_US)
				result[path.join('/')] = +!!file.de_DE

			for (let key in file) {
				if (key.includes(' ')) {
					file[key.replace(/ /g, '')] = file[key]
					delete file[key]
					key = key.replace(/ /g, '')
				}
				path.push(key)
				dig_find_text(file[key], path, result)
				path.pop()
			}
			return result
		}
		let v = dig_find_text(this)
		this.__has_text = !!Object.keys(v).length
		this.__text_paths = v
		return this
	}

	getNode(npath) {
		return !npath ? this : npath.split('/').reduce((v, e) => {
			if (v[e] || v.hasOwnProperty(e))
				return v[e]
			return Object.fromEntries(Object.keys(v).filter(k => k.startsWith(e + '.')).map(k => [k.slice(e.length + 1), v[k]]))
		}, this)
	}

	replaceDeutschWithEnglish(addPaths) {
		this.findTexts()
		for (let npath of Object.keys(this.__text_paths)) {
			let node = this.getNode(npath)
			if (CrossFile.keepLangs) {
				node.de_WS = node.de_WS || node.de_DE
			} else {
				delete node.fr_FR
				delete node.ja_JP
				delete node.ko_KR
				delete node.zh_CN
			}
			node.de_DE = node.en_US
			node.__path = this.__path + ' ' + npath
		}
		return this
	}

	_getNodeInfo(node, index, nodes, npaths) {
		if (node.type == 'IF') {
			let step = npaths[index + 1].split('.').pop()
			return node.type + ' ' + (step == 'thenStep' ? '' : step == 'elseStep' ? 'NOT ' : step + ' ') + node.condition
		}
		if (node.type == 'EventTrigger') {
			let next = nodes[index + 1]
			nodes[index + 1] = {}
			return node.type + ' ' + next.name + ' ' + (next.condition || next.startCondition || '')
		}
		if (node.person) {
			return node.person.person + ' @' + node.person.expression + ' ' + //
				(Object.keys(node.person).length > 2 ? JSON.stringify(Object.fromEntries( //
					Object.entries(node.person).filter(e => !'person,expression,de_DE,zh_CN,ja_JP,ko_KR,de_WS'.split(',').includes(e[0])) //
				)) : '')
		}

		if (node.type) {
			if (typeof node.type == 'object')
				//WTF?
				return JSON.stringify(node.type)
			return node.type
		}

		if (node.condition || node.startCondition)
			if (typeof(node.condition || node.startCondition) == 'string')
				return node.condition || node.startCondition
	}

	getNodeInfo(npath) {
		let node = this.getNode(npath)
		let npaths = npath.split('/').map((e, i, a) => a.slice(0, i).join('/'))
		npaths.push(npath)
		//.map(e=>this.getNode(e))
		let nodes = npaths.map(e => this.getNode(e))

		let v = nodes.map((e, i, a) => this._getNodeInfo(e, i, a, npaths))

		let p = this;

		// console.log({
		//     npath,
		//     node,
		//     nodes,
		//     n: Object.fromEntries(npaths.map((e,i)=>[e, nodes[i]])),
		//     v: Object.fromEntries(npaths.map((e,i)=>[e, v[i]])),
		//     langUid: node.langUid,
		//     en_US: node.en_US
		// })
		let pathData = v.filter(e => e && !e.match(/^\s*$/)).join('\n')
		let data = `${this.__path}.json ${npath} #${node.langUid}\n${pathData}${pathData && '\n'}\n${node.en_US}`
		return data;
		// console.log(data)
	}

	generateNodeInfos() {
		this.findTexts()
		return Object.keys(this.__text_paths).map(e => this.getNodeInfo(e))
	}

	generateEnRus(force = false) {
		if (this.infos && !force)
			return this.infos
		let infos = this.generateNodeInfos()
		let infoERs = infos.map((e, i) => new CrossEnRu().setEn(e).setIndex(i + 1).setFile(this))
		let map = new Map(infoERs.map(e => [e.path, e]))
		Object.defineValue(this, 'infoMap', map)
		// let set = new Set()
		// infoERs = infoERs.filter(e=>!set.has(e.uniq)&&set.add(e.uniq))

		let header = new CrossEnRu().setRaw(`${this.__path.replace(/./g, '-')}\n\n${this.__path}\n\n${this.__path.replace(/./g, '-')}`).setIndex(0).setFile(this)
		Object.defineValue(this, 'infos', [header, ...infoERs])
		return this.infos
	}
	static generateLocationPaths(locName) {
		if (!this.generatedMapPaths) {
			let a = CrossFile.findTextFiles().filter(e => e.includes('/areas/')).map(e => CrossFile.at(e).load())
			let b = a.map(f => f.floors.flatMap(e => e.maps).map(e => e.path).map(e => e.split('.')[0]).filter((e, i, a) => a[i - 1] != e))
			let c = a.map(f => f.floors.flatMap(e => e.maps).map(e => e.path).map(e => `data/maps/${e.replace(/\./g, '/')}`).filter(e => CrossFile.at(e)))
			c = a.map((e, i) => [e.__path].concat(c[i]))
			let set = new Set(c.flat())
			c = c.map(e => e.filter(e => set.delete(e)))
			let o = Object.fromEntries(c.flat().map((e, i) => [e, i + 1]))
			this.generatedMapPaths = o
		}
		let d = this.generatedMapPaths
		let e0 = CrossFile.findTextFiles().map(e => CrossFile.at(e).__path)
		let e = e0.filter(e => e.includes(`/${locName}/`))
		let e1 = e.filter(e => d[e]).sort((a, b) => d[a] - d[b])
		let e2 = e.filter(e => !d[e])
		e = [].concat(e1, e2)
		if (locName != 'enemies')
			e = e.filter(e => !e.includes('/enemies/'))
		if (this.at('data/areas/' + locName))
			e.unshift('data/areas/' + locName)
		if (!e.length && this.at('data/' + locName))
			e.push('data/' + locName)
		if (locName == 'etc') {
			e = CrossFile.findTextFiles()//.filter(e => !e.includes('/credits/'))
			.map(e => CrossFile.at(e)).filter(e => !e.__area || e.__area == 'etc').map(e => e.__path)
		}
		if (e[0] && CrossFile.at(e[0]).__area) {
			let f = CrossFile.at(e[0])
			e.map(e => CrossFile.at(e).__area = f.__area)
		} else {
			e.map(e => CrossFile.at(e).__area = locName)
		}
		// console.log(locName, e)
		return e
	}

	static generateLocation(locName) {
		let paths = this.generateLocationPaths(locName)
		return paths.map((e, i) => CrossFile.at(e).load().generateEnRus().map(e => e.setFileIndex(i))).flat()
	}

	static mergeLang() {
		function dig_join_lang(file, key = '') {
			if (typeof file == 'string') {
				if (key.startsWith('__') || ['DOCTYPE', 'feature'].includes(key))
					return file
				return {
					en_US: file,
					de_DE: file
				}
			}
			if (typeof file != 'object' || !file)
				return file

			return Object.fromEntries(Object.entries(file).map(([k, v]) => [k, dig_join_lang(v, k)]))
		}

		let fnames = ['map-content', 'gui', 'gimmick']

		let p = new CrossFile('data/LANG').__full_path
		fs.writeFileSync(p, '{}')
		let flang = CrossFile.at(p).load()
		flang.sc = {}

		for (let fname of fnames) {
			let f = new CrossFile('data/lang/sc/' + fname + '.en_US').loadBackup()
			let ff = dig_join_lang(f)
			ff.__path = ff.__path.replace('en_US', 'de_DE')
			flang.sc[f.__path.split('/').pop().split('.')[0]] = ff
		}
		flang.replaceDeutschWithEnglish()
		fs.writeFileSync(flang.__back_path, JSON.stringify(flang))

		return flang.loadBackup().save()
	}

	static splitLang() {
		let flang = CrossFile.at('data/LANG')
		flang.__loaded || flang.load()
		let splitted = []

		for (let [k, v] of Object.entries(flang.sc)) {

			let f = CrossFile.at(v.__path).load()

			Object.setPrototypeOf(v, CrossFile.prototype)
			v.findTexts()

			for (let path in v.__text_paths) {

				let fNode = path.split('/').slice(0, -1).reduce((v, e) => v[e], f)
				let fKey = path.split('/').pop()
				let node = v.getNode(path)

				if (fNode[fKey] != node.de_DE) {
					fNode[fKey] = node.de_DE
					f.__modified = flang.__modified;
				}

			}
			splitted.push(f);
		}

		return splitted
	}

}
