yatl = {
	get_key(ls = false) {
		let k = localStorage.yatl_key
		if (!k || !k.match(/^trnsl(.\w+)+$/)) {
			if (ls)
				return false

			k = prompt('Нужен ключ от Yandex Translate API')
			localStorage.yatl_key = k
			return this.get_key(true)
		}
		return k
	},
	get key() {
		return this.get_key()
	},
	text: 'And that\'s how the <c>Temple Mine</c> was born.',
	lang: 'en-ru',
	format: 'html',

	get h() {
		return `
			https://translate.yandex.net/api/v1.5/tr.json/translate
			?key=${this.key}
			&text=${encodeURI(this.text)}
			&lang=${this.lang}
			&format=${this.format}
		`.replace(/\s*\n\s*/g, '')
	},

	async translate() {
		if (!this.key) return
		let r = await fetch(this.h)
		let j = await r.json()
		log(j)
		return j.text[0]
	},

	async translate_cell(cell) {
		let t = cell.q('.tl-en-text').innerText
		t = t.replace(/\\[a-z](\[[^\]]+\])?/g, '')
		log({ t })
		yatl.text = t
		let r = await yatl.translate()
		if (!r) throw new Error('Failed to translate')
		log({ t, r })
		cell.q('textarea').value = r
		cell.q('textarea').oninput()
	},

	async multitl_cell(cell) {
		let langs = 'en_US,de_WS,zh_CN,ja_JP,ko_KR'.split(',')

		let file = cell.q('.tl-en-file').innerText
		let path = cell.q('.tl-en-path').innerText

		let n = CrossFile.at(file).getNode(path)

		let r = langs.filter(l => n[l]).pmap(async l => {
			yatl.lang = l.slice(0, 2) + '-ru'
			yatl.text = n[l].replace(/\\[a-z](\[[^\]]+\])?/g, '').split('<<')[0]
			return [l, await yatl.translate()]
		})
		yatl.lang = 'en-ru'
		r = await r
		log({ n, r })

		cell.q('textarea').value = r.map(e => e.join('\n')).join('\n')
		cell.q('textarea').oninput()
	},

	async onkeydown(ev) {
		if (!ev.ctrlKey || ev.code != 'KeyT')
			return
		ev.preventDefault()

		if (!ev.target || !ev.target.matches('textarea.tl-en-edit'))
			return

		if (!ev.shiftKey) {
			this.translate_cell(ev.target.parentNode)
		} else {
			this.multitl_cell(ev.target.parentNode)
		}

	},

	bind(d) {
		if (d.getAttribute('yatl') == 'bind')
			return false
		d.setAttribute('yatl', 'bind')
		d.addEventListener('keydown', ev => this.onkeydown(ev))
		return true
	},

	unbind(d) {
		if (d.getAttribute('yatl') != 'bind')
			return false
		d.setAttribute('yatl', '')
		d.removeEventListener('keydown', ev => this.onkeydown(ev))
		return true
	},
}